# recdata

This application is a service to create dynamics forms using NativeScript RadDataForm JSON format,
collect samples in Apache CouchDB and manage credentials

## Acknowledgements

- [Cloudant IBM](https://www.ibm.com/cloud/cloudant)
- [NativeScript - RadDataForm - Describe the Properties](https://v7.docs.nativescript.org/angular/ui/ng-components/ng-dataform/gettingstarted/dataform-start-properties#adjust-editors-with-json)
- [Apache CouchDB](https://couchdb.apache.org/)
- [Cloudant IBM](https://www.ibm.com/cloud/cloudant)
- [@angular/cdk/drag-drop](https://material.angular.io/cdk/drag-drop/overview)

## Development

Before you can build this project, you must install and configure the following dependencies on your machine:

1. [Node.js][]: We use Node to run a development web server and build the project.
   Depending on your system, you can install Node either from source or as a pre-packaged bundle.

After installing Node, you should be able to run the following command to install development tools.
You will only need to run this command when dependencies change in [package.json](package.json).

    npm install

`CouchDB is required to run application`, if you don't have couchDB instance can start one with [docker file dev.yml](#using-docker-to-simplify-development-optional)

Run the following commands in two separate terminals to create a blissful development experience where your browser
auto-refreshes when files change on your hard drive.

    ./mvnw
    npm start

Npm is also used to manage CSS and JavaScript dependencies used in this application. You can upgrade dependencies by
specifying a newer version in [package.json](package.json). You can also run `npm update` and `npm install` to manage dependencies.
Add the `help` flag on any command to see how you can use it. For example, `npm help update`.

The `npm run` command will list all the scripts available to run for this project.

## Building for production

### Packaging as jar

To build the final jar and optimize the recdata application for production, run:

    ./mvnw -Pprod clean verify

This will concatenate and minify the client CSS and JavaScript files. It will also modify `index.html` so it references these new files.
To ensure everything worked, run:

    java -jar target/*.jar

Then navigate to [http://localhost:8080](http://localhost:8080) in your browser.

### Packaging as war

To package your application as a war in order to deploy it to an application server, run:

    ./mvnw -Pprod,war clean verify

## Using Docker to simplify development (optional)

To start a mysql and couchDB database in a docker container, run:

    docker-compose -f src/main/docker/dev.yml up -d

Then run:

    ./mvnw

To stop it and remove the container, run:

    docker-compose -f src/main/docker/dev.yml down

You can also fully dockerize your application and all the services that it depends on.
To achieve this, first build a docker image of your app by running:

    ./mvnw -Pprod verify jib:dockerBuild

Or run for all service in one:

    docker-compose -f src/main/docker/app.yml up -d

## Source structure

#### src/main/docker

Several docker YML files for development and production environment
include script to generate ssl certificate for https (certs/nginx_cert.sh)

#### src/main/java

Source application that content domains, controllers and services java files

#### src/main/resources

Configuration files, frontend (Angular) and liquibase files

## API Reference

#### Model form -> CouchDB

| Package              | Class                                                                            | Description                    |
| :------------------- | :------------------------------------------------------------------------------- | :----------------------------- |
| `com.roviol.service` | [`CouchDBService`](src/main/java/com/roviol/service/CouchDBService.java)         | Manager CouchDB connections    |
| `com.roviol.service` | [`FormularioCouchDB`](src/main/java/com/roviol/converter/FormularioCouchDB.java) | Model RadDataForm              |
| `com.roviol.service` | [`SchemaFormService`](src/main/java/com/roviol/service/SchemaFormService.java)   | Create JSON Metadata for forms |

#### Get form

```http
  GET /formulario
```

| endpoing    | Description                              |
| :---------- | :--------------------------------------- |
| `{id}/view` | View form                                |
| `{id}/edit` | Edit form (using @angular/cdk/drag-drop) |

![Form Editor](assets/form.png)

## Architecture

![Flow](assets/architecture.png)

## Tech Stack

**Client:** Angular

**Server:** Java, MySQL, CouchDB
