import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IListado } from 'app/shared/model/listado.model';

@Component({
  selector: 'jhi-listado-detail',
  templateUrl: './listado-detail.component.html'
})
export class ListadoDetailComponent implements OnInit {
  listado: IListado;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ listado }) => {
      this.listado = listado;
    });
  }

  previousState() {
    window.history.back();
  }
}
