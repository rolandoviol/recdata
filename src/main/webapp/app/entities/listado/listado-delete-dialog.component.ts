import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IListado } from 'app/shared/model/listado.model';
import { ListadoService } from './listado.service';

@Component({
  selector: 'jhi-listado-delete-dialog',
  templateUrl: './listado-delete-dialog.component.html'
})
export class ListadoDeleteDialogComponent {
  listado: IListado;

  constructor(protected listadoService: ListadoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.listadoService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'listadoListModification',
        content: 'Deleted an listado'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-listado-delete-popup',
  template: ''
})
export class ListadoDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ listado }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ListadoDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.listado = listado;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/listado', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/listado', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
