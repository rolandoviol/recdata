import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IListado } from 'app/shared/model/listado.model';

type EntityResponseType = HttpResponse<IListado>;
type EntityArrayResponseType = HttpResponse<IListado[]>;

@Injectable({ providedIn: 'root' })
export class ListadoService {
  public resourceUrl = SERVER_API_URL + 'api/listados';

  constructor(protected http: HttpClient) {}

  create(listado: IListado): Observable<EntityResponseType> {
    return this.http.post<IListado>(this.resourceUrl, listado, { observe: 'response' });
  }

  update(listado: IListado): Observable<EntityResponseType> {
    return this.http.put<IListado>(this.resourceUrl, listado, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IListado>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IListado[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
