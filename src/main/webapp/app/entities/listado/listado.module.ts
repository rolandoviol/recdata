import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { RecdataSharedModule } from 'app/shared';
import {
  ListadoComponent,
  ListadoDetailComponent,
  ListadoUpdateComponent,
  ListadoDeletePopupComponent,
  ListadoDeleteDialogComponent,
  listadoRoute,
  listadoPopupRoute
} from './';

const ENTITY_STATES = [...listadoRoute, ...listadoPopupRoute];

@NgModule({
  imports: [RecdataSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ListadoComponent,
    ListadoDetailComponent,
    ListadoUpdateComponent,
    ListadoDeleteDialogComponent,
    ListadoDeletePopupComponent
  ],
  entryComponents: [ListadoComponent, ListadoUpdateComponent, ListadoDeleteDialogComponent, ListadoDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecdataListadoModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
