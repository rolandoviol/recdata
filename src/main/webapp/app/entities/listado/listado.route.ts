import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Listado } from 'app/shared/model/listado.model';
import { ListadoService } from './listado.service';
import { ListadoComponent } from './listado.component';
import { ListadoDetailComponent } from './listado-detail.component';
import { ListadoUpdateComponent } from './listado-update.component';
import { ListadoDeletePopupComponent } from './listado-delete-dialog.component';
import { IListado } from 'app/shared/model/listado.model';

@Injectable({ providedIn: 'root' })
export class ListadoResolve implements Resolve<IListado> {
  constructor(private service: ListadoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IListado> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Listado>) => response.ok),
        map((listado: HttpResponse<Listado>) => listado.body)
      );
    }
    return of(new Listado());
  }
}

export const listadoRoute: Routes = [
  {
    path: '',
    component: ListadoComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'recdataApp.listado.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ListadoDetailComponent,
    resolve: {
      listado: ListadoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.listado.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ListadoUpdateComponent,
    resolve: {
      listado: ListadoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.listado.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ListadoUpdateComponent,
    resolve: {
      listado: ListadoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.listado.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const listadoPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ListadoDeletePopupComponent,
    resolve: {
      listado: ListadoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.listado.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
