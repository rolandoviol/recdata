import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Listadet } from 'app/shared/model/listadet.model';
import { ListadetService } from './listadet.service';
import { ListadetComponent } from './listadet.component';
import { ListadetDetailComponent } from './listadet-detail.component';
import { ListadetUpdateComponent } from './listadet-update.component';
import { ListadetDeletePopupComponent } from './listadet-delete-dialog.component';
import { IListadet } from 'app/shared/model/listadet.model';

@Injectable({ providedIn: 'root' })
export class ListadetResolve implements Resolve<IListadet> {
  constructor(private service: ListadetService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IListadet> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Listadet>) => response.ok),
        map((listadet: HttpResponse<Listadet>) => listadet.body)
      );
    }
    return of(new Listadet());
  }
}

export const listadetRoute: Routes = [
  {
    path: '',
    component: ListadetComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'recdataApp.listadet.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: ListadetDetailComponent,
    resolve: {
      listadet: ListadetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.listadet.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: ListadetUpdateComponent,
    resolve: {
      listadet: ListadetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.listadet.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: ListadetUpdateComponent,
    resolve: {
      listadet: ListadetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.listadet.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const listadetPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: ListadetDeletePopupComponent,
    resolve: {
      listadet: ListadetResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.listadet.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
