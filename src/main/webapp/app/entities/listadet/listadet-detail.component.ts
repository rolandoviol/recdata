import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IListadet } from 'app/shared/model/listadet.model';

@Component({
  selector: 'jhi-listadet-detail',
  templateUrl: './listadet-detail.component.html'
})
export class ListadetDetailComponent implements OnInit {
  listadet: IListadet;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ listadet }) => {
      this.listadet = listadet;
    });
  }

  previousState() {
    window.history.back();
  }
}
