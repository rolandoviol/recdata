import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IListadet } from 'app/shared/model/listadet.model';
import { ListadetService } from './listadet.service';

@Component({
  selector: 'jhi-listadet-delete-dialog',
  templateUrl: './listadet-delete-dialog.component.html'
})
export class ListadetDeleteDialogComponent {
  listadet: IListadet;

  constructor(protected listadetService: ListadetService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.listadetService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'listadetListModification',
        content: 'Deleted an listadet'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-listadet-delete-popup',
  template: ''
})
export class ListadetDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ listadet }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(ListadetDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.listadet = listadet;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/listadet', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/listadet', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
