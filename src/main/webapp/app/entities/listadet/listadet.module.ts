import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { RecdataSharedModule } from 'app/shared';
import {
  ListadetComponent,
  ListadetDetailComponent,
  ListadetUpdateComponent,
  ListadetDeletePopupComponent,
  ListadetDeleteDialogComponent,
  listadetRoute,
  listadetPopupRoute
} from './';

const ENTITY_STATES = [...listadetRoute, ...listadetPopupRoute];

@NgModule({
  imports: [RecdataSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    ListadetComponent,
    ListadetDetailComponent,
    ListadetUpdateComponent,
    ListadetDeleteDialogComponent,
    ListadetDeletePopupComponent
  ],
  entryComponents: [ListadetComponent, ListadetUpdateComponent, ListadetDeleteDialogComponent, ListadetDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecdataListadetModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
