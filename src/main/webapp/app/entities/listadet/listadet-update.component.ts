import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { IListadet, Listadet } from 'app/shared/model/listadet.model';
import { ListadetService } from './listadet.service';
import { IListado } from 'app/shared/model/listado.model';
import { ListadoService } from 'app/entities/listado';

@Component({
  selector: 'jhi-listadet-update',
  templateUrl: './listadet-update.component.html'
})
export class ListadetUpdateComponent implements OnInit {
  isSaving: boolean;

  listados: IListado[];

  editForm = this.fb.group({
    id: [],
    name: [null, [Validators.required]],
    listado: [null, Validators.required]
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected listadetService: ListadetService,
    protected listadoService: ListadoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ listadet }) => {
      this.updateForm(listadet);
    });
    this.listadoService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IListado[]>) => mayBeOk.ok),
        map((response: HttpResponse<IListado[]>) => response.body)
      )
      .subscribe((res: IListado[]) => (this.listados = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(listadet: IListadet) {
    this.editForm.patchValue({
      id: listadet.id,
      name: listadet.name,
      listado: listadet.listado
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const listadet = this.createFromForm();
    if (listadet.id !== undefined) {
      this.subscribeToSaveResponse(this.listadetService.update(listadet));
    } else {
      this.subscribeToSaveResponse(this.listadetService.create(listadet));
    }
  }

  private createFromForm(): IListadet {
    return {
      ...new Listadet(),
      id: this.editForm.get(['id']).value,
      name: this.editForm.get(['name']).value,
      listado: this.editForm.get(['listado']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IListadet>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackListadoById(index: number, item: IListado) {
    return item.id;
  }
}
