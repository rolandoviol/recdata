import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IListadet } from 'app/shared/model/listadet.model';

type EntityResponseType = HttpResponse<IListadet>;
type EntityArrayResponseType = HttpResponse<IListadet[]>;

@Injectable({ providedIn: 'root' })
export class ListadetService {
  public resourceUrl = SERVER_API_URL + 'api/listadets';

  constructor(protected http: HttpClient) {}

  create(listadet: IListadet): Observable<EntityResponseType> {
    return this.http.post<IListadet>(this.resourceUrl, listadet, { observe: 'response' });
  }

  update(listadet: IListadet): Observable<EntityResponseType> {
    return this.http.put<IListadet>(this.resourceUrl, listadet, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IListadet>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IListadet[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
