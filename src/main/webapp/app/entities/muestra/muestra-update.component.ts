import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { JhiAlertService, JhiDataUtils } from 'ng-jhipster';
import { IMuestra, Muestra } from 'app/shared/model/muestra.model';
import { MuestraService } from './muestra.service';
import { IFormulario } from 'app/shared/model/formulario.model';
import { FormularioService } from 'app/entities/formulario';
import { IUser, UserService } from 'app/core';

@Component({
  selector: 'jhi-muestra-update',
  templateUrl: './muestra-update.component.html'
})
export class MuestraUpdateComponent implements OnInit {
  isSaving: boolean;

  formularios: IFormulario[];

  users: IUser[];
  inicioDp: any;

  editForm = this.fb.group({
    id: [],
    nombre: [null, [Validators.required]],
    logo: [],
    logoContentType: [],
    inicio: [],
    activo: [null, [Validators.required]],
    formulario: [],
    user: []
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected jhiAlertService: JhiAlertService,
    protected muestraService: MuestraService,
    protected formularioService: FormularioService,
    protected userService: UserService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ muestra }) => {
      this.updateForm(muestra);
    });
    this.formularioService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IFormulario[]>) => mayBeOk.ok),
        map((response: HttpResponse<IFormulario[]>) => response.body)
      )
      .subscribe((res: IFormulario[]) => (this.formularios = res), (res: HttpErrorResponse) => this.onError(res.message));
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(muestra: IMuestra) {
    this.editForm.patchValue({
      id: muestra.id,
      nombre: muestra.nombre,
      logo: muestra.logo,
      logoContentType: muestra.logoContentType,
      inicio: muestra.inicio,
      activo: muestra.activo,
      formulario: muestra.formulario,
      user: muestra.user
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }

  setFileData(event, field: string, isImage) {
    return new Promise((resolve, reject) => {
      if (event && event.target && event.target.files && event.target.files[0]) {
        const file = event.target.files[0];
        if (isImage && !/^image\//.test(file.type)) {
          reject(`File was expected to be an image but was found to be ${file.type}`);
        } else {
          const filedContentType: string = field + 'ContentType';
          this.dataUtils.toBase64(file, base64Data => {
            this.editForm.patchValue({
              [field]: base64Data,
              [filedContentType]: file.type
            });
          });
        }
      } else {
        reject(`Base64 data was not set as file could not be extracted from passed parameter: ${event}`);
      }
    }).then(
      () => console.log('blob added'), // sucess
      this.onError
    );
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string) {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null
    });
    if (this.elementRef && idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const muestra = this.createFromForm();
    if (muestra.id !== undefined) {
      this.subscribeToSaveResponse(this.muestraService.update(muestra));
    } else {
      this.subscribeToSaveResponse(this.muestraService.create(muestra));
    }
  }

  private createFromForm(): IMuestra {
    return {
      ...new Muestra(),
      id: this.editForm.get(['id']).value,
      nombre: this.editForm.get(['nombre']).value,
      logoContentType: this.editForm.get(['logoContentType']).value,
      logo: this.editForm.get(['logo']).value,
      inicio: this.editForm.get(['inicio']).value,
      activo: this.editForm.get(['activo']).value,
      formulario: this.editForm.get(['formulario']).value,
      user: this.editForm.get(['user']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMuestra>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackFormularioById(index: number, item: IFormulario) {
    return item.id;
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }
}
