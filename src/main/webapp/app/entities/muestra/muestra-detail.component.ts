import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IMuestra } from 'app/shared/model/muestra.model';

@Component({
  selector: 'jhi-muestra-detail',
  templateUrl: './muestra-detail.component.html'
})
export class MuestraDetailComponent implements OnInit {
  muestra: IMuestra;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ muestra }) => {
      this.muestra = muestra;
    });
  }

  byteSize(field) {
    return this.dataUtils.byteSize(field);
  }

  openFile(contentType, field) {
    return this.dataUtils.openFile(contentType, field);
  }
  previousState() {
    window.history.back();
  }
}
