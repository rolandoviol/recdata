import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IMuestra } from 'app/shared/model/muestra.model';

type EntityResponseType = HttpResponse<IMuestra>;
type EntityArrayResponseType = HttpResponse<IMuestra[]>;

@Injectable({ providedIn: 'root' })
export class MuestraService {
  public resourceUrl = SERVER_API_URL + 'api/muestras';

  constructor(protected http: HttpClient) {}

  create(muestra: IMuestra): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(muestra);
    return this.http
      .post<IMuestra>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(muestra: IMuestra): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(muestra);
    return this.http
      .put<IMuestra>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMuestra>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMuestra[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  patch(id: number): Observable<HttpResponse<any>> {
    return this.http.patch<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(muestra: IMuestra): IMuestra {
    const copy: IMuestra = Object.assign({}, muestra, {
      inicio: muestra.inicio != null && muestra.inicio.isValid() ? muestra.inicio.format(DATE_FORMAT) : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.inicio = res.body.inicio != null ? moment(res.body.inicio) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((muestra: IMuestra) => {
        muestra.inicio = muestra.inicio != null ? moment(muestra.inicio) : null;
      });
    }
    return res;
  }
}
