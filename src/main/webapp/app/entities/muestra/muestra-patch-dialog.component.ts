import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IMuestra } from 'app/shared/model/muestra.model';
import { MuestraService } from './muestra.service';

@Component({
  selector: 'jhi-muestra-patch-dialog',
  templateUrl: './muestra-patch-dialog.component.html'
})
export class MuestraPatchDialogComponent {
  muestra: IMuestra;

  constructor(protected muestraService: MuestraService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmPatch(id: number) {
    this.muestraService.patch(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'muestraListModification',
        content: 'Patchd an muestra'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-muestra-patch-popup',
  template: ''
})
export class MuestraPatchPopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ muestra }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(MuestraPatchDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.muestra = muestra;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/muestra', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/muestra', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
