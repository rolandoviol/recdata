import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { JhiPaginationUtil, JhiResolvePagingParams } from 'ng-jhipster';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Muestra } from 'app/shared/model/muestra.model';
import { MuestraService } from './muestra.service';
import { MuestraComponent } from './muestra.component';
import { MuestraDetailComponent } from './muestra-detail.component';
import { MuestraUpdateComponent } from './muestra-update.component';
import { MuestraDeletePopupComponent } from './muestra-delete-dialog.component';
import { MuestraPatchPopupComponent } from './muestra-patch-dialog.component';
import { IMuestra } from 'app/shared/model/muestra.model';

@Injectable({ providedIn: 'root' })
export class MuestraResolve implements Resolve<IMuestra> {
  constructor(private service: MuestraService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IMuestra> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Muestra>) => response.ok),
        map((muestra: HttpResponse<Muestra>) => muestra.body)
      );
    }
    return of(new Muestra());
  }
}

export const muestraRoute: Routes = [
  {
    path: '',
    component: MuestraComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: ['ROLE_USER'],
      defaultSort: 'id,asc',
      pageTitle: 'recdataApp.muestra.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: MuestraDetailComponent,
    resolve: {
      muestra: MuestraResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.muestra.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: MuestraUpdateComponent,
    resolve: {
      muestra: MuestraResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.muestra.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: MuestraUpdateComponent,
    resolve: {
      muestra: MuestraResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.muestra.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const muestraPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: MuestraDeletePopupComponent,
    resolve: {
      muestra: MuestraResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.muestra.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  },
  {
    path: ':id/patch',
    component: MuestraPatchPopupComponent,
    resolve: {
      muestra: MuestraResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.muestra.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
