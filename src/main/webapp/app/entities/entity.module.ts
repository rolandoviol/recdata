import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'formulario',
        loadChildren: () => import('./formulario/formulario.module').then(m => m.RecdataFormularioModule)
      },
      {
        path: 'campo',
        loadChildren: () => import('./campo/campo.module').then(m => m.RecdataCampoModule)
      },
      {
        path: 'muestra',
        loadChildren: () => import('./muestra/muestra.module').then(m => m.RecdataMuestraModule)
      },
      {
        path: 'registro',
        loadChildren: () => import('./registro/registro.module').then(m => m.RecdataRegistroModule)
      },
      {
        path: 'perfil',
        loadChildren: () => import('./perfil/perfil.module').then(m => m.RecdataPerfilModule)
      },
      {
        path: 'listado',
        loadChildren: () => import('./listado/listado.module').then(m => m.RecdataListadoModule)
      },
      {
        path: 'listadet',
        loadChildren: () => import('./listadet/listadet.module').then(m => m.RecdataListadetModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ],
  declarations: [],
  entryComponents: [],
  providers: [],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecdataEntityModule {}
