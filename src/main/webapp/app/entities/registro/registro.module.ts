import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { RecdataSharedModule } from 'app/shared';
import {
  RegistroComponent,
  RegistroDetailComponent,
  RegistroUpdateComponent,
  RegistroDeletePopupComponent,
  RegistroDeleteDialogComponent,
  registroRoute,
  registroPopupRoute
} from './';

const ENTITY_STATES = [...registroRoute, ...registroPopupRoute];

@NgModule({
  imports: [RecdataSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [
    RegistroComponent,
    RegistroDetailComponent,
    RegistroUpdateComponent,
    RegistroDeleteDialogComponent,
    RegistroDeletePopupComponent
  ],
  entryComponents: [RegistroComponent, RegistroUpdateComponent, RegistroDeleteDialogComponent, RegistroDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecdataRegistroModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
