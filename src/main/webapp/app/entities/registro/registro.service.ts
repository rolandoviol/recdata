import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { map } from 'rxjs/operators';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IRegistro } from 'app/shared/model/registro.model';

type EntityResponseType = HttpResponse<IRegistro>;
type EntityArrayResponseType = HttpResponse<IRegistro[]>;

@Injectable({ providedIn: 'root' })
export class RegistroService {
  public resourceUrl = SERVER_API_URL + 'api/registros';

  constructor(protected http: HttpClient) {}

  create(registro: IRegistro): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(registro);
    return this.http
      .post<IRegistro>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(registro: IRegistro): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(registro);
    return this.http
      .put<IRegistro>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRegistro>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRegistro[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(registro: IRegistro): IRegistro {
    const copy: IRegistro = Object.assign({}, registro, {
      fecha: registro.fecha != null && registro.fecha.isValid() ? registro.fecha.toJSON() : null
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fecha = res.body.fecha != null ? moment(res.body.fecha) : null;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((registro: IRegistro) => {
        registro.fecha = registro.fecha != null ? moment(registro.fecha) : null;
      });
    }
    return res;
  }
}
