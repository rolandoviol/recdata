import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IRegistro } from 'app/shared/model/registro.model';
import { AccountService } from 'app/core';
import { RegistroService } from './registro.service';

@Component({
  selector: 'jhi-registro',
  templateUrl: './registro.component.html'
})
export class RegistroComponent implements OnInit, OnDestroy {
  registros: IRegistro[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected registroService: RegistroService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.registroService
      .query()
      .pipe(
        filter((res: HttpResponse<IRegistro[]>) => res.ok),
        map((res: HttpResponse<IRegistro[]>) => res.body)
      )
      .subscribe(
        (res: IRegistro[]) => {
          this.registros = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInRegistros();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IRegistro) {
    return item.id;
  }

  registerChangeInRegistros() {
    this.eventSubscriber = this.eventManager.subscribe('registroListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
