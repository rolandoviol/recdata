import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiAlertService } from 'ng-jhipster';
import { IRegistro, Registro } from 'app/shared/model/registro.model';
import { RegistroService } from './registro.service';
import { IMuestra } from 'app/shared/model/muestra.model';
import { MuestraService } from 'app/entities/muestra';

@Component({
  selector: 'jhi-registro-update',
  templateUrl: './registro-update.component.html'
})
export class RegistroUpdateComponent implements OnInit {
  isSaving: boolean;

  muestras: IMuestra[];

  editForm = this.fb.group({
    id: [],
    nombre: [null, [Validators.required]],
    fecha: [null, [Validators.required]],
    borrado: [null, [Validators.required]],
    muestra: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected registroService: RegistroService,
    protected muestraService: MuestraService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ registro }) => {
      this.updateForm(registro);
    });
    this.muestraService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IMuestra[]>) => mayBeOk.ok),
        map((response: HttpResponse<IMuestra[]>) => response.body)
      )
      .subscribe((res: IMuestra[]) => (this.muestras = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(registro: IRegistro) {
    this.editForm.patchValue({
      id: registro.id,
      nombre: registro.nombre,
      fecha: registro.fecha != null ? registro.fecha.format(DATE_TIME_FORMAT) : null,
      borrado: registro.borrado,
      muestra: registro.muestra
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const registro = this.createFromForm();
    if (registro.id !== undefined) {
      this.subscribeToSaveResponse(this.registroService.update(registro));
    } else {
      this.subscribeToSaveResponse(this.registroService.create(registro));
    }
  }

  private createFromForm(): IRegistro {
    return {
      ...new Registro(),
      id: this.editForm.get(['id']).value,
      nombre: this.editForm.get(['nombre']).value,
      fecha: this.editForm.get(['fecha']).value != null ? moment(this.editForm.get(['fecha']).value, DATE_TIME_FORMAT) : undefined,
      borrado: this.editForm.get(['borrado']).value,
      muestra: this.editForm.get(['muestra']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRegistro>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackMuestraById(index: number, item: IMuestra) {
    return item.id;
  }
}
