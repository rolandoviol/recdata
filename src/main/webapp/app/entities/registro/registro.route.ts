import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Registro } from 'app/shared/model/registro.model';
import { RegistroService } from './registro.service';
import { RegistroComponent } from './registro.component';
import { RegistroDetailComponent } from './registro-detail.component';
import { RegistroUpdateComponent } from './registro-update.component';
import { RegistroDeletePopupComponent } from './registro-delete-dialog.component';
import { IRegistro } from 'app/shared/model/registro.model';

@Injectable({ providedIn: 'root' })
export class RegistroResolve implements Resolve<IRegistro> {
  constructor(private service: RegistroService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IRegistro> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Registro>) => response.ok),
        map((registro: HttpResponse<Registro>) => registro.body)
      );
    }
    return of(new Registro());
  }
}

export const registroRoute: Routes = [
  {
    path: '',
    component: RegistroComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.registro.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: RegistroDetailComponent,
    resolve: {
      registro: RegistroResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.registro.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: RegistroUpdateComponent,
    resolve: {
      registro: RegistroResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.registro.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: RegistroUpdateComponent,
    resolve: {
      registro: RegistroResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.registro.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const registroPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: RegistroDeletePopupComponent,
    resolve: {
      registro: RegistroResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.registro.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
