import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IRegistro } from 'app/shared/model/registro.model';
import { RegistroService } from './registro.service';

@Component({
  selector: 'jhi-registro-delete-dialog',
  templateUrl: './registro-delete-dialog.component.html'
})
export class RegistroDeleteDialogComponent {
  registro: IRegistro;

  constructor(protected registroService: RegistroService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.registroService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'registroListModification',
        content: 'Deleted an registro'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-registro-delete-popup',
  template: ''
})
export class RegistroDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ registro }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(RegistroDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.registro = registro;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/registro', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/registro', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
