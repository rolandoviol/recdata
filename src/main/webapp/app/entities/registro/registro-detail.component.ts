import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRegistro } from 'app/shared/model/registro.model';

@Component({
  selector: 'jhi-registro-detail',
  templateUrl: './registro-detail.component.html'
})
export class RegistroDetailComponent implements OnInit {
  registro: IRegistro;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ registro }) => {
      this.registro = registro;
    });
  }

  previousState() {
    window.history.back();
  }
}
