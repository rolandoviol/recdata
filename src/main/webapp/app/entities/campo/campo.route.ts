import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Campo } from 'app/shared/model/campo.model';
import { CampoService } from './campo.service';
import { CampoComponent } from './campo.component';
import { CampoDetailComponent } from './campo-detail.component';
import { CampoUpdateComponent } from './campo-update.component';
import { CampoDeletePopupComponent } from './campo-delete-dialog.component';
import { ICampo } from 'app/shared/model/campo.model';
import { Formulario } from 'app/shared/model/formulario.model';
import { FormularioService } from 'app/entities/formulario';

@Injectable({ providedIn: 'root' })
export class CampoResolve implements Resolve<ICampo> {
  constructor(private service: CampoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICampo> {
    const id = route.params['id'];
    const form = route.params['form'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Campo>) => response.ok),
        map((campo: HttpResponse<Campo>) => campo.body)
      );
    }
    if (!form) {
      return of(new Campo());
    } else {
      return of(new Campo(undefined, undefined, undefined, undefined, undefined, undefined, new Formulario(form)));
    }
  }
}

@Injectable({ providedIn: 'root' })
export class FormResolve implements Resolve<ICampo> {
  constructor(private serviceForm: FormularioService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICampo> {
    const id = route.params['form'];
    if (id) {
      return this.serviceForm.find(id).pipe(
        filter((response: HttpResponse<Formulario>) => response.ok),
        map((formulario: HttpResponse<Formulario>) => formulario.body)
      );
    }
  }
}

export const campoRoute: Routes = [
  {
    path: '',
    component: CampoComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.campo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: CampoDetailComponent,
    resolve: {
      campo: CampoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.campo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: CampoUpdateComponent,
    resolve: {
      campo: CampoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.campo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },

  {
    path: ':form/add',
    component: CampoUpdateComponent,
    resolve: {
      campo: CampoResolve,
      formulario: FormResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.campo.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: CampoUpdateComponent,
    resolve: {
      campo: CampoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.campo.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const campoPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: CampoDeletePopupComponent,
    resolve: {
      campo: CampoResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.campo.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
