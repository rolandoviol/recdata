import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { RecdataSharedModule } from 'app/shared';
import {
  CampoComponent,
  CampoDetailComponent,
  CampoUpdateComponent,
  CampoDeletePopupComponent,
  CampoDeleteDialogComponent,
  campoRoute,
  campoPopupRoute
} from './';

const ENTITY_STATES = [...campoRoute, ...campoPopupRoute];

@NgModule({
  imports: [RecdataSharedModule, RouterModule.forChild(ENTITY_STATES)],
  declarations: [CampoComponent, CampoDetailComponent, CampoUpdateComponent, CampoDeleteDialogComponent, CampoDeletePopupComponent],
  entryComponents: [CampoComponent, CampoUpdateComponent, CampoDeleteDialogComponent, CampoDeletePopupComponent],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecdataCampoModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
