import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { ICampo } from 'app/shared/model/campo.model';
import { AccountService } from 'app/core';
import { CampoService } from './campo.service';

@Component({
  selector: 'jhi-campo',
  templateUrl: './campo.component.html'
})
export class CampoComponent implements OnInit, OnDestroy {
  campos: ICampo[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected campoService: CampoService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.campoService
      .query()
      .pipe(
        filter((res: HttpResponse<ICampo[]>) => res.ok),
        map((res: HttpResponse<ICampo[]>) => res.body)
      )
      .subscribe(
        (res: ICampo[]) => {
          this.campos = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInCampos();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: ICampo) {
    return item.id;
  }

  registerChangeInCampos() {
    this.eventSubscriber = this.eventManager.subscribe('campoListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
