import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICampo } from 'app/shared/model/campo.model';

@Component({
  selector: 'jhi-campo-detail',
  templateUrl: './campo-detail.component.html'
})
export class CampoDetailComponent implements OnInit {
  campo: ICampo;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ campo }) => {
      this.campo = campo;
    });
  }

  previousState() {
    window.history.back();
  }
}
