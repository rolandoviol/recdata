import { Component, OnInit } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { ICampo, Campo } from 'app/shared/model/campo.model';
import { CampoService } from './campo.service';
import { IFormulario } from 'app/shared/model/formulario.model';
import { FormularioService } from 'app/entities/formulario';
import { IListado } from 'app/shared/model/listado.model';
import { ListadoService } from 'app/entities/listado';

@Component({
  selector: 'jhi-campo-update',
  templateUrl: './campo-update.component.html'
})
export class CampoUpdateComponent implements OnInit {
  isSaving: boolean;

  formularios: IFormulario[];

  listados: IListado[];

  editForm = this.fb.group({
    id: [],
    nombreCampo: [null, [Validators.required]],
    descripcion: [],
    tipo: [null, [Validators.required]],
    requerido: [null, [Validators.required]],
    posicion: [],
    formulario: [],
    listado: []
  });

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected campoService: CampoService,
    protected formularioService: FormularioService,
    protected listadoService: ListadoService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ campo, formulario }) => {
      if (formulario !== undefined) {
        if (campo.id === undefined) {
          campo.formulario = formulario;
        }
      }
      this.updateForm(campo);
    });
    this.formularioService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IFormulario[]>) => mayBeOk.ok),
        map((response: HttpResponse<IFormulario[]>) => response.body)
      )
      .subscribe((res: IFormulario[]) => (this.formularios = res), (res: HttpErrorResponse) => this.onError(res.message));
  }

  updateForm(campo: ICampo) {
    this.editForm.patchValue({
      id: campo.id,
      nombreCampo: campo.nombreCampo,
      descripcion: campo.descripcion,
      tipo: campo.tipo,
      requerido: campo.requerido,
      posicion: campo.posicion,
      formulario: campo.formulario,
      listado: campo.listado
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const campo = this.createFromForm();
    if (campo.id !== undefined) {
      this.subscribeToSaveResponse(this.campoService.update(campo));
    } else {
      this.subscribeToSaveResponse(this.campoService.create(campo));
    }
  }

  private createFromForm(): ICampo {
    return {
      ...new Campo(),
      id: this.editForm.get(['id']).value,
      nombreCampo: this.editForm.get(['nombreCampo']).value,
      descripcion: this.editForm.get(['descripcion']).value,
      tipo: this.editForm.get(['tipo']).value,
      requerido: this.editForm.get(['requerido']).value,
      posicion: this.editForm.get(['posicion']).value,
      formulario: this.editForm.get(['formulario']).value,
      listado: this.editForm.get(['listado']).value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICampo>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackFormularioById(index: number, item: IFormulario) {
    return item.id;
  }

  trackListadoById(index: number, item: IListado) {
    return item.id;
  }
}
