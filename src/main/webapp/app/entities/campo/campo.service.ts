import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { ICampo } from 'app/shared/model/campo.model';

type EntityResponseType = HttpResponse<ICampo>;
type EntityArrayResponseType = HttpResponse<ICampo[]>;

@Injectable({ providedIn: 'root' })
export class CampoService {
  public resourceUrl = SERVER_API_URL + 'api/campos';

  constructor(protected http: HttpClient) {}

  create(campo: ICampo): Observable<EntityResponseType> {
    return this.http.post<ICampo>(this.resourceUrl, campo, { observe: 'response' });
  }

  update(campo: ICampo): Observable<EntityResponseType> {
    return this.http.put<ICampo>(this.resourceUrl, campo, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICampo>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICampo[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  findForm(id: number): Observable<EntityArrayResponseType> {
    return this.http.get<ICampo[]>(`${this.resourceUrl}/form/${id}`, { observe: 'response' });
  }
}
