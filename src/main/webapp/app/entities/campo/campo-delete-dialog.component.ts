import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { ICampo } from 'app/shared/model/campo.model';
import { CampoService } from './campo.service';

@Component({
  selector: 'jhi-campo-delete-dialog',
  templateUrl: './campo-delete-dialog.component.html'
})
export class CampoDeleteDialogComponent {
  campo: ICampo;

  constructor(protected campoService: CampoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.campoService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'campoListModification',
        content: 'Deleted an campo'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-campo-delete-popup',
  template: ''
})
export class CampoDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ campo }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(CampoDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.campo = campo;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/campo', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/campo', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
