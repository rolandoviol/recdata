import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';
import { JhiLanguageService } from 'ng-jhipster';
import { JhiLanguageHelper } from 'app/core';

import { RecdataSharedModule } from 'app/shared';
import {
  FormularioComponent,
  FormularioDetailComponent,
  FormularioUpdateComponent,
  FormularioDeletePopupComponent,
  FormularioDeleteDialogComponent,
  formularioRoute,
  formularioPopupRoute
} from './';
import { FormularioCampoDeleteDialogComponent } from './formulario-campo-delete-dialog/formulario-campo-delete-dialog.component';
import { DragDropModule } from '@angular/cdk/drag-drop';

const ENTITY_STATES = [...formularioRoute, ...formularioPopupRoute];

@NgModule({
  imports: [RecdataSharedModule, RouterModule.forChild(ENTITY_STATES), DragDropModule],
  declarations: [
    FormularioComponent,
    FormularioDetailComponent,
    FormularioUpdateComponent,
    FormularioDeleteDialogComponent,
    FormularioDeletePopupComponent,
    FormularioCampoDeleteDialogComponent
  ],
  entryComponents: [
    FormularioComponent,
    FormularioUpdateComponent,
    FormularioDeleteDialogComponent,
    FormularioDeletePopupComponent,
    FormularioCampoDeleteDialogComponent
  ],
  providers: [{ provide: JhiLanguageService, useClass: JhiLanguageService }],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecdataFormularioModule {
  constructor(private languageService: JhiLanguageService, private languageHelper: JhiLanguageHelper) {
    this.languageHelper.language.subscribe((languageKey: string) => {
      if (languageKey) {
        this.languageService.changeLanguage(languageKey);
      }
    });
  }
}
