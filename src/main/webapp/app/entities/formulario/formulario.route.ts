import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes } from '@angular/router';
import { UserRouteAccessService } from 'app/core';
import { Observable, of } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { Formulario } from 'app/shared/model/formulario.model';
import { FormularioService } from './formulario.service';
import { FormularioComponent } from './formulario.component';
import { FormularioDetailComponent } from './formulario-detail.component';
import { FormularioUpdateComponent } from './formulario-update.component';
import { FormularioDeletePopupComponent } from './formulario-delete-dialog.component';
import { IFormulario } from 'app/shared/model/formulario.model';
import { CampoService } from 'app/entities/campo';
import { Campo, ICampo } from 'app/shared/model/campo.model';

@Injectable({ providedIn: 'root' })
export class FormularioResolve implements Resolve<IFormulario> {
  constructor(private service: FormularioService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<IFormulario> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        filter((response: HttpResponse<Formulario>) => response.ok),
        map((formulario: HttpResponse<Formulario>) => formulario.body)
      );
    }
    return of(new Formulario());
  }
}

@Injectable({ providedIn: 'root' })
export class CampoResolve implements Resolve<ICampo> {
  constructor(private service: CampoService) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ICampo> {
    const idcampo = route.params['idcampo'];
    if (idcampo) {
      return this.service.find(idcampo).pipe(
        filter((response: HttpResponse<Campo>) => response.ok),
        map((campo: HttpResponse<Campo>) => campo.body)
      );
    }
    return of(new Campo());
  }
}

export const formularioRoute: Routes = [
  {
    path: '',
    component: FormularioComponent,
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.formulario.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: FormularioDetailComponent,
    resolve: {
      formulario: FormularioResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.formulario.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: FormularioUpdateComponent,
    resolve: {
      formulario: FormularioResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.formulario.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: FormularioUpdateComponent,
    resolve: {
      formulario: FormularioResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.formulario.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view/',
    component: FormularioDetailComponent,
    resolve: {
      formulario: FormularioResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.formulario.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

export const formularioPopupRoute: Routes = [
  {
    path: ':id/delete',
    component: FormularioDeletePopupComponent,
    resolve: {
      formulario: FormularioResolve
    },
    data: {
      authorities: ['ROLE_USER'],
      pageTitle: 'recdataApp.formulario.home.title'
    },
    canActivate: [UserRouteAccessService],
    outlet: 'popup'
  }
];
