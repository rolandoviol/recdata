import { Component, OnInit } from '@angular/core';
import { ICampo } from 'app/shared/model/campo.model';
import { CampoService } from 'app/entities/campo';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

@Component({
  selector: 'jhi-formulario-campo-delete-dialog',
  templateUrl: './formulario-campo-delete-dialog.component.html',
  styleUrls: ['./formulario-campo-delete-dialog.component.scss']
})
export class FormularioCampoDeleteDialogComponent implements OnInit {
  campo: ICampo;

  constructor(protected campoService: CampoService, public activeModal: NgbActiveModal, protected eventManager: JhiEventManager) {}

  ngOnInit() {}

  confirmDelete(id: any) {
    this.campoService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'campoListModification',
        content: 'Deleted an campo'
      });
      this.activeModal.dismiss('done');
    });
  }

  clear() {
    this.activeModal.dismiss('cancel');
  }
}
