import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IFormulario } from 'app/shared/model/formulario.model';
import { filter, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { CampoService } from 'app/entities/campo';
import { ICampo } from 'app/shared/model/campo.model';
import { JhiAlertService } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormularioCampoDeleteDialogComponent } from 'app/entities/formulario/formulario-campo-delete-dialog/formulario-campo-delete-dialog.component';

@Component({
  selector: 'jhi-formulario-detail',
  templateUrl: './formulario-detail.component.html'
})
export class FormularioDetailComponent implements OnInit {
  formulario: IFormulario;
  campos: ICampo[];

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected campoService: CampoService,
    protected jhiAlertService: JhiAlertService,
    private _modalService: NgbModal
  ) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ formulario }) => {
      this.formulario = formulario;
      this.loadCampos();
    });
  }

  loadCampos() {
    this.campoService
      .findForm(this.formulario.id)
      .pipe(
        filter((res: HttpResponse<ICampo[]>) => res.ok),
        map((res: HttpResponse<ICampo[]>) => res.body)
      )
      .subscribe(
        (res: ICampo[]) => {
          this.campos = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  previousState() {
    window.history.back();
  }

  private onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackId(index: number, item: ICampo) {
    return item.id;
  }

  open(campo: ICampo) {
    const modalRef = this._modalService.open(FormularioCampoDeleteDialogComponent);
    modalRef.componentInstance.campo = campo;
    modalRef.result.then(
      result => {
        if (result) {
          console.log(result);
        }
      },
      reason => {
        if (reason) {
          if (reason === 'done') {
            this.loadCampos();
          }
        }
      }
    );
  }
}
