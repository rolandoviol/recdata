import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IFormulario } from 'app/shared/model/formulario.model';
import { AccountService } from 'app/core';
import { FormularioService } from './formulario.service';

@Component({
  selector: 'jhi-formulario',
  templateUrl: './formulario.component.html'
})
export class FormularioComponent implements OnInit, OnDestroy {
  formularios: IFormulario[];
  currentAccount: any;
  eventSubscriber: Subscription;

  constructor(
    protected formularioService: FormularioService,
    protected jhiAlertService: JhiAlertService,
    protected eventManager: JhiEventManager,
    protected accountService: AccountService
  ) {}

  loadAll() {
    this.formularioService
      .query()
      .pipe(
        filter((res: HttpResponse<IFormulario[]>) => res.ok),
        map((res: HttpResponse<IFormulario[]>) => res.body)
      )
      .subscribe(
        (res: IFormulario[]) => {
          this.formularios = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  ngOnInit() {
    this.loadAll();
    this.accountService.identity().then(account => {
      this.currentAccount = account;
    });
    this.registerChangeInFormularios();
  }

  ngOnDestroy() {
    this.eventManager.destroy(this.eventSubscriber);
  }

  trackId(index: number, item: IFormulario) {
    return item.id;
  }

  registerChangeInFormularios() {
    this.eventSubscriber = this.eventManager.subscribe('formularioListModification', response => this.loadAll());
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }
}
