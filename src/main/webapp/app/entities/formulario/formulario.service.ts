import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared';
import { IFormulario } from 'app/shared/model/formulario.model';

type EntityResponseType = HttpResponse<IFormulario>;
type EntityArrayResponseType = HttpResponse<IFormulario[]>;

@Injectable({ providedIn: 'root' })
export class FormularioService {
  public resourceUrl = SERVER_API_URL + 'api/formularios';

  constructor(protected http: HttpClient) {}

  create(formulario: IFormulario): Observable<EntityResponseType> {
    return this.http.post<IFormulario>(this.resourceUrl, formulario, { observe: 'response' });
  }

  update(formulario: IFormulario): Observable<EntityResponseType> {
    return this.http.put<IFormulario>(this.resourceUrl, formulario, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFormulario>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFormulario[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<any>> {
    return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }
}
