import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, CdkDragEnter, CdkDragExit, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';
import { JhiAlertService } from 'ng-jhipster';
import { Formulario, IFormulario } from 'app/shared/model/formulario.model';
import { FormularioService } from './formulario.service';
import { IUser, UserService } from 'app/core';
import { Campo, ICampo, TIPO } from 'app/shared/model/campo.model';
import { CampoService } from 'app/entities/campo';

@Component({
  selector: 'jhi-formulario-update',
  templateUrl: './formulario-update.component.html'
})
export class FormularioUpdateComponent implements OnInit {
  isSaving: boolean;
  campos: ICampo[];
  camposDelete: Campo[] = [];
  camposel: ICampo;
  formulario_id: number;
  users: IUser[];
  indiceActual = -1;

  tiposdedatos = {
    grupo01: [TIPO.Text, TIPO.MultilineText, TIPO.Email, TIPO.Password],
    grupo02: [TIPO.Phone, TIPO.Number, TIPO.Decimal, TIPO.Switch, TIPO.Stepper, TIPO.Slider],
    grupo03: [TIPO.Picker, TIPO.SegmentedEditor, TIPO.List],
    grupo04: [TIPO.DatePicker, TIPO.TimePicker],
    grupo05: [TIPO.AutoCompleteInline, TIPO.Label]
  };

  editForm = this.fb.group({
    id: [],
    nombre: [null, [Validators.required]],
    descripcion: [],
    revision: [],
    user: []
  });

  editCampo = this.fb.group({
    id: [],
    nombreCampo: [null, [Validators.required]],
    descripcion: [],
    tipo: [null, [Validators.required]],
    requerido: [null, [Validators.required]],
    posicion: [],
    formulario: []
  });
  overBorrado: boolean;

  constructor(
    protected jhiAlertService: JhiAlertService,
    protected formularioService: FormularioService,
    protected campoService: CampoService,
    protected userService: UserService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.isSaving = false;
    this.activatedRoute.data.subscribe(({ formulario }) => {
      this.formulario_id = formulario.id;
      this.updateForm(formulario);
      if (this.formulario_id) {
        this.loadCampos();
      }
    });
    this.userService
      .query()
      .pipe(
        filter((mayBeOk: HttpResponse<IUser[]>) => mayBeOk.ok),
        map((response: HttpResponse<IUser[]>) => response.body)
      )
      .subscribe((res: IUser[]) => (this.users = res), (res: HttpErrorResponse) => this.onError(res.message));

    this.editCampo.valueChanges.subscribe(val => {
      this.campos[this.indiceActual] = val;
    });
  }

  updateForm(formulario: IFormulario) {
    this.editForm.patchValue({
      id: formulario.id,
      nombre: formulario.nombre,
      descripcion: formulario.descripcion,
      revision: formulario.revision,
      user: formulario.user
    });
  }

  updateCampo(campo: ICampo) {
    this.editCampo.patchValue({
      id: campo.id,
      nombreCampo: campo.nombreCampo,
      descripcion: campo.descripcion,
      tipo: campo.tipo,
      requerido: campo.requerido,
      posicion: campo.posicion,
      formulario: campo.formulario
    });
  }

  previousState() {
    window.history.back();
  }

  save() {
    this.isSaving = true;
    const formulario = this.createFromForm();
    formulario.campos = this.campos;
    if (this.camposDelete.length > 0) {
      console.log(this.camposDelete.length);
      const campoSer = this.campoService;
      this.camposDelete.forEach(function(campoDel) {
        campoSer.delete(campoDel.id).subscribe();
      });
    }
    if (formulario.id !== undefined) {
      this.subscribeToSaveResponse(this.formularioService.update(formulario));
    } else {
      this.subscribeToSaveResponse(this.formularioService.create(formulario));
    }
  }

  private createFromForm(): IFormulario {
    return {
      ...new Formulario(),
      id: this.editForm.get(['id']).value,
      nombre: this.editForm.get(['nombre']).value,
      descripcion: this.editForm.get(['descripcion']).value,
      revision: this.editForm.get(['revision']).value,
      user: this.editForm.get(['user']).value
    };
  }

  private createCampo(nombre, vdescripcion, vtipo, vformulario): ICampo {
    return {
      ...new Campo(),
      id: null,
      nombreCampo: nombre,
      descripcion: vdescripcion,
      tipo: vtipo,
      requerido: false,
      posicion: null,
      formulario: vformulario,
      listado: null
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFormulario>>) {
    result.subscribe(() => this.onSaveSuccess(), () => this.onSaveError());
  }

  protected onSaveSuccess() {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError() {
    this.isSaving = false;
  }
  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackUserById(index: number, item: IUser) {
    return item.id;
  }

  loadCampos() {
    this.campoService
      .findForm(this.formulario_id)
      .pipe(
        filter((res: HttpResponse<ICampo[]>) => res.ok),
        map((res: HttpResponse<ICampo[]>) => res.body)
      )
      .subscribe(
        (res: ICampo[]) => {
          this.campos = res;
        },
        (res: HttpErrorResponse) => this.onError(res.message)
      );
  }

  trackIdCampo(index: number, item: ICampo) {
    return item.id;
  }

  selCampo(campo: ICampo, indice: number) {
    this.indiceActual = indice;
    this.camposel = campo;
    this.updateCampo(campo);
  }

  dropOrder(event: CdkDragDrop<ICampo[], any>) {
    if (event.container === event.previousContainer) {
      moveItemInArray(this.campos, event.previousIndex, event.currentIndex);
    } else {
      const tipo = this.indexToTipo(event.previousIndex);

      const formulario = this.createFromForm();
      const v_nombre = this.verificaNombre(tipo);
      const campolocal = this.createCampo(v_nombre, v_nombre, tipo, formulario);
      const camposlocal = [campolocal];
      transferArrayItem(camposlocal, event.container.data, 0, event.currentIndex);
    }
    this.indiceActual = -1;
    this.actualizaPosicion();
  }

  dropBorrar(event: CdkDragDrop<ICampo[], any>) {
    const campoBorrar = this.campos[event.previousIndex];

    if (campoBorrar.id !== null) {
      this.camposDelete.push(campoBorrar);
    }
    this.campos.splice(event.previousIndex, 1);
    console.log(campoBorrar);
    this.actualizaPosicion();
    this.overBorrado = false;
  }

  borrarEnter(event: CdkDragEnter<any>) {
    this.overBorrado = true;
  }

  borrarExit(event: CdkDragExit<any>) {
    this.overBorrado = false;
  }

  actualizaPosicion() {
    for (let pos = 0; pos < this.campos.length; pos++) {
      if (this.campos[pos].posicion !== -1) {
        this.campos[pos].posicion = pos;
      }
    }
  }

  verificaNombre(nombre) {
    let intentos = 0;
    let v_nombre = nombre;
    for (let pos = 0; pos < this.campos.length; pos++) {
      if (this.campos[pos].nombreCampo === v_nombre) {
        intentos++;
        v_nombre = v_nombre + ' ' + intentos;
      }
    }
    return v_nombre;
  }

  indexToTipo(indice) {
    let indicegrupo = indice;
    let lenrevision = this.tiposdedatos.grupo01.length;
    if (indice < lenrevision) {
      return this.tiposdedatos.grupo01[indicegrupo];
    }

    lenrevision = lenrevision + this.tiposdedatos.grupo02.length;
    indicegrupo = indicegrupo - this.tiposdedatos.grupo01.length;
    if (indice < lenrevision) {
      return this.tiposdedatos.grupo02[indicegrupo];
    }

    lenrevision = lenrevision + this.tiposdedatos.grupo03.length;
    indicegrupo = indicegrupo - this.tiposdedatos.grupo02.length;
    if (indice < lenrevision) {
      return this.tiposdedatos.grupo03[indicegrupo];
    }

    lenrevision = lenrevision + this.tiposdedatos.grupo04.length;
    indicegrupo = indicegrupo - this.tiposdedatos.grupo03.length;
    if (indice < lenrevision) {
      return this.tiposdedatos.grupo04[indicegrupo];
    }

    lenrevision = lenrevision + this.tiposdedatos.grupo05.length;
    indicegrupo = indicegrupo - this.tiposdedatos.grupo04.length;
    if (indice < lenrevision) {
      return this.tiposdedatos.grupo05[indicegrupo];
    }
  }
}
