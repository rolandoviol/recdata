import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbActiveModal, NgbModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IFormulario } from 'app/shared/model/formulario.model';
import { FormularioService } from './formulario.service';

@Component({
  selector: 'jhi-formulario-delete-dialog',
  templateUrl: './formulario-delete-dialog.component.html'
})
export class FormularioDeleteDialogComponent {
  formulario: IFormulario;

  constructor(
    protected formularioService: FormularioService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  clear() {
    this.activeModal.dismiss('cancel');
  }

  confirmDelete(id: number) {
    this.formularioService.delete(id).subscribe(response => {
      this.eventManager.broadcast({
        name: 'formularioListModification',
        content: 'Deleted an formulario'
      });
      this.activeModal.dismiss(true);
    });
  }
}

@Component({
  selector: 'jhi-formulario-delete-popup',
  template: ''
})
export class FormularioDeletePopupComponent implements OnInit, OnDestroy {
  protected ngbModalRef: NgbModalRef;

  constructor(protected activatedRoute: ActivatedRoute, protected router: Router, protected modalService: NgbModal) {}

  ngOnInit() {
    this.activatedRoute.data.subscribe(({ formulario }) => {
      setTimeout(() => {
        this.ngbModalRef = this.modalService.open(FormularioDeleteDialogComponent as Component, { size: 'lg', backdrop: 'static' });
        this.ngbModalRef.componentInstance.formulario = formulario;
        this.ngbModalRef.result.then(
          result => {
            this.router.navigate(['/formulario', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          },
          reason => {
            this.router.navigate(['/formulario', { outlets: { popup: null } }]);
            this.ngbModalRef = null;
          }
        );
      }, 0);
    });
  }

  ngOnDestroy() {
    this.ngbModalRef = null;
  }
}
