import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RecdataSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective } from './';

@NgModule({
  imports: [RecdataSharedCommonModule],
  declarations: [JhiLoginModalComponent, HasAnyAuthorityDirective],
  entryComponents: [JhiLoginModalComponent],
  exports: [RecdataSharedCommonModule, JhiLoginModalComponent, HasAnyAuthorityDirective],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class RecdataSharedModule {
  static forRoot() {
    return {
      ngModule: RecdataSharedModule
    };
  }
}
