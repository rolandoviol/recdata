import { Moment } from 'moment';
import { IMuestra } from 'app/shared/model/muestra.model';

export interface IRegistro {
  id?: number;
  nombre?: string;
  fecha?: Moment;
  borrado?: boolean;
  muestra?: IMuestra;
}

export class Registro implements IRegistro {
  constructor(public id?: number, public nombre?: string, public fecha?: Moment, public borrado?: boolean, public muestra?: IMuestra) {
    this.borrado = this.borrado || false;
  }
}
