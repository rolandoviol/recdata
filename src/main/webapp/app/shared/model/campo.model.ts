import { IFormulario } from 'app/shared/model/formulario.model';
import { IListado } from 'app/shared/model/listado.model';

export const enum TIPO {
  Text = 'Text',
  MultilineText = 'MultilineText',
  Email = 'Email',
  Password = 'Password',
  Phone = 'Phone',
  Number = 'Number',
  Decimal = 'Decimal',
  Switch = 'Switch',
  Stepper = 'Stepper',
  Slider = 'Slider',
  Picker = 'Picker',
  SegmentedEditor = 'SegmentedEditor',
  List = 'List',
  DatePicker = 'DatePicker',
  TimePicker = 'TimePicker',
  AutoCompleteInline = 'AutoCompleteInline',
  Label = 'Label'
}

export interface ICampo {
  id?: number;
  nombreCampo?: string;
  descripcion?: string;
  tipo?: TIPO;
  requerido?: boolean;
  posicion?: number;
  formulario?: IFormulario;
  listado?: IListado;
}

export class Campo implements ICampo {
  constructor(
    public id?: number,
    public nombreCampo?: string,
    public descripcion?: string,
    public tipo?: TIPO,
    public requerido?: boolean,
    public posicion?: number,
    public formulario?: IFormulario,
    public listado?: IListado
  ) {
    this.requerido = this.requerido || false;
  }
}
