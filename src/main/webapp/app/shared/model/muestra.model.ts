import { Moment } from 'moment';
import { IRegistro } from 'app/shared/model/registro.model';
import { IFormulario } from 'app/shared/model/formulario.model';
import { IUser } from 'app/core/user/user.model';

export interface IMuestra {
  id?: number;
  nombre?: string;
  logoContentType?: string;
  logo?: any;
  inicio?: Moment;
  activo?: boolean;
  databaseName?: string;
  databaseKey?: string;
  databaseSecret?: string;
  ocupado?: number;
  registros?: IRegistro[];
  formulario?: IFormulario;
  user?: IUser;
}

export class Muestra implements IMuestra {
  constructor(
    public id?: number,
    public nombre?: string,
    public logoContentType?: string,
    public logo?: any,
    public inicio?: Moment,
    public activo?: boolean,
    public databaseName?: string,
    public databaseKey?: string,
    public databaseSecret?: string,
    public ocupado?: number,
    public registros?: IRegistro[],
    public formulario?: IFormulario,
    public user?: IUser
  ) {
    this.activo = this.activo || false;
  }
}
