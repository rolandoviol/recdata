import { IListado } from 'app/shared/model/listado.model';

export interface IListadet {
  id?: number;
  name?: string;
  listado?: IListado;
}

export class Listadet implements IListadet {
  constructor(public id?: number, public name?: string, public listado?: IListado) {}
}
