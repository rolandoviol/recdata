import { IUser } from 'app/core/user/user.model';

export interface IListado {
  id?: number;
  name?: string;
  user?: IUser;
}

export class Listado implements IListado {
  constructor(public id?: number, public name?: string, public user?: IUser) {}
}
