import { ICampo } from 'app/shared/model/campo.model';
import { IMuestra } from 'app/shared/model/muestra.model';
import { IUser } from 'app/core/user/user.model';

export interface IFormulario {
  id?: number;
  nombre?: string;
  descripcion?: string;
  revision?: string;
  campos?: ICampo[];
  muestras?: IMuestra[];
  user?: IUser;
}

export class Formulario implements IFormulario {
  constructor(
    public id?: number,
    public nombre?: string,
    public descripcion?: string,
    public revision?: string,
    public campos?: ICampo[],
    public muestras?: IMuestra[],
    public user?: IUser
  ) {}
}
