import { IUser } from 'app/core/user/user.model';

export interface IPerfil {
  id?: number;
  secret?: string;
  user?: IUser;
}

export class Perfil implements IPerfil {
  constructor(public id?: number, public secret?: string, public user?: IUser) {}
}
