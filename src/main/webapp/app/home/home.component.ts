import { Component, OnInit } from '@angular/core';
import { NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { JhiAlertService, JhiEventManager, JhiParseLinks } from 'ng-jhipster';

import { LoginModalService, AccountService, Account } from 'app/core';
import { MuestraService } from 'app/entities/muestra';
import { IMuestra } from 'app/shared/model/muestra.model';
import { HttpErrorResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { IFormulario } from 'app/shared/model/formulario.model';
import { ActivatedRoute, Router } from '@angular/router';
import { ITEMS_PER_PAGE } from 'app/shared';
import { FormularioService } from 'app/entities/formulario';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit {
  muestras: IMuestra[];
  formularios: IFormulario[];
  account: Account;
  modalRef: NgbModalRef;

  routeData: any;
  links: any;
  totalItems: any;
  itemsPerPage: any;
  page: any;
  predicate: any;
  previousPage: any;
  reverse: any;

  linksF: any;
  totalItemsF: any;
  pageF: any;

  constructor(
    private accountService: AccountService,
    private loginModalService: LoginModalService,
    private eventManager: JhiEventManager,
    protected muestraService: MuestraService,
    protected formularioService: FormularioService,
    protected router: Router,
    protected parseLinks: JhiParseLinks,
    protected activatedRoute: ActivatedRoute,
    protected jhiAlertService: JhiAlertService
  ) {
    this.itemsPerPage = ITEMS_PER_PAGE;
    this.routeData = this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.previousPage = data.pagingParams.page;
      this.reverse = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
    });
  }

  ngOnInit() {
    this.accountService.identity().then((account: Account) => {
      this.account = account;
      this.loadHome();
    });
    this.registerAuthenticationSuccess();
  }

  registerAuthenticationSuccess() {
    this.eventManager.subscribe('authenticationSuccess', message => {
      this.accountService.identity().then(account => {
        this.account = account;
        this.loadHome();
      });
    });
  }

  isAuthenticated() {
    return this.accountService.isAuthenticated();
  }

  login() {
    this.modalRef = this.loginModalService.open();
  }

  loadHome() {
    if (this.isAuthenticated()) {
      this.muestraService
        .query({
          page: this.page - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe(
          (res: HttpResponse<IMuestra[]>) => this.paginateMuestras(res.body, res.headers),
          (res: HttpErrorResponse) => this.onError(res.message)
        );

      this.formularioService
        .query({
          page: this.pageF - 1,
          size: this.itemsPerPage,
          sort: this.sort()
        })
        .subscribe(
          (res: HttpResponse<IFormulario[]>) => this.paginateFormularios(res.body, res.headers),
          (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
  }

  protected onError(errorMessage: string) {
    this.jhiAlertService.error(errorMessage, null, null);
  }

  trackIdMuestra(index: number, item: IMuestra) {
    return item.id;
  }

  loadPage(page: number) {
    if (page !== this.previousPage) {
      this.previousPage = page;
      this.transition();
    }
  }

  transition() {
    this.router.navigate(['/'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    });
    this.loadHome();
  }

  clear() {
    this.page = 0;
    this.router.navigate([
      '/',
      {
        page: this.page,
        sort: this.predicate + ',' + (this.reverse ? 'asc' : 'desc')
      }
    ]);
    this.loadHome();
  }

  sort() {
    const result = [this.predicate + ',' + (this.reverse ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  protected paginateMuestras(data: IMuestra[], headers: HttpHeaders) {
    this.links = this.parseLinks.parse(headers.get('link'));
    this.totalItems = parseInt(headers.get('X-Total-Count'), 10);
    this.muestras = data;
  }

  protected paginateFormularios(data: IFormulario[], headers: HttpHeaders) {
    this.linksF = this.parseLinks.parse(headers.get('link'));
    this.totalItemsF = parseInt(headers.get('X-Total-Count'), 10);
    this.formularios = data;
  }
}
