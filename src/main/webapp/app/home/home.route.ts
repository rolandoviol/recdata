import { Route } from '@angular/router';

import { HomeComponent } from './';
import { JhiResolvePagingParams } from 'ng-jhipster';

export const HOME_ROUTE: Route = {
  path: '',
  component: HomeComponent,
  resolve: {
    pagingParams: JhiResolvePagingParams
  },
  data: {
    authorities: [],
    defaultSort: 'id,asc',
    pageTitle: 'home.title'
  }
};
