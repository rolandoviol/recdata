package com.roviol.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.roviol.service.util.ConvertUtil;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;

import com.roviol.domain.enumeration.TIPO;
import org.springframework.data.annotation.ReadOnlyProperty;

/**
 * A Campo.
 */
@Entity
@Table(name = "campo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Campo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nombre_campo", nullable = false)
    private String nombreCampo;

    @Column(name = "descripcion")
    private String descripcion;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo", nullable = false)
    private TIPO tipo;

    @NotNull
    @Column(name = "requerido", nullable = false)
    private Boolean requerido;

    @Column(name = "posicion")
    private Integer posicion;

    @ManyToOne
    @JsonIgnoreProperties("campos")
    private Formulario formulario;

    @ManyToOne
    @JsonIgnoreProperties("campos")
    private Listado listado;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    @ReadOnlyProperty
    @Column(name = "fieldname")
    private String fieldname;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreCampo() {
        return nombreCampo;
    }

    public Campo nombreCampo(String nombreCampo) {
        this.nombreCampo = nombreCampo;
        if (this.fieldname == null) {
            this.fieldname = ConvertUtil.toField(nombreCampo);
        }
        return this;
    }

    public void setNombreCampo(String nombreCampo) {
        this.nombreCampo = nombreCampo;
        if (this.fieldname == null) {
            this.fieldname = ConvertUtil.toField(nombreCampo);
        }
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Campo descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public TIPO getTipo() {
        return tipo;
    }

    public Campo tipo(TIPO tipo) {
        this.tipo = tipo;
        return this;
    }

    public void setTipo(TIPO tipo) {
        this.tipo = tipo;
    }

    public Boolean isRequerido() {
        return requerido;
    }

    public Campo requerido(Boolean requerido) {
        this.requerido = requerido;
        return this;
    }

    public void setRequerido(Boolean requerido) {
        this.requerido = requerido;
    }

    public Integer getPosicion() {
        return posicion;
    }

    public Campo posicion(Integer posicion) {
        this.posicion = posicion;
        return this;
    }

    public void setPosicion(Integer posicion) {
        this.posicion = posicion;
    }

    public Formulario getFormulario() {
        return formulario;
    }

    public Campo formulario(Formulario formulario) {
        this.formulario = formulario;
        return this;
    }

    public void setFormulario(Formulario formulario) {
        this.formulario = formulario;
    }

    public Listado getListado() {
        return listado;
    }

    public Campo listado(Listado listado) {
        this.listado = listado;
        return this;
    }

    public void setListado(Listado listado) {
        this.listado = listado;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    public String getFieldname() {
        return fieldname;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Campo)) {
            return false;
        }
        return id != null && id.equals(((Campo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Campo{" +
            "id=" + getId() +
            ", nombreCampo='" + getNombreCampo() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", requerido='" + isRequerido() + "'" +
            ", posicion=" + getPosicion() +
            ", fieldname='" + getFieldname() + "'" +
            "}";
    }

}
