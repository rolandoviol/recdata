package com.roviol.domain;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Muestra.
 */
@Entity
@Table(name = "muestra")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Muestra implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Lob
    @Column(name = "logo")
    private byte[] logo;

    @Column(name = "logo_content_type")
    private String logoContentType;

    @Column(name = "inicio")
    private LocalDate inicio;

    @NotNull
    @Column(name = "activo", nullable = false)
    private Boolean activo;

    @Column(name = "database_name")
    private String databaseName;

    @JsonIgnore
    @Column(name = "database_key", unique = true)
    private String databaseKey;

    @JsonIgnore
    @Column(name = "database_secret")
    private String databaseSecret;

    @Column(name = "ocupado")
    private Long ocupado;

    @OneToMany(mappedBy = "muestra")
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Registro> registros = new HashSet<>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnoreProperties("muestras")
    private Formulario formulario;

    @ManyToOne
    @JsonIgnoreProperties("muestras")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Muestra nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public byte[] getLogo() {
        return logo;
    }

    public Muestra logo(byte[] logo) {
        this.logo = logo;
        return this;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public Muestra logoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
        return this;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public LocalDate getInicio() {
        return inicio;
    }

    public Muestra inicio(LocalDate inicio) {
        this.inicio = inicio;
        return this;
    }

    public void setInicio(LocalDate inicio) {
        this.inicio = inicio;
    }

    public Boolean isActivo() {
        return activo;
    }

    public Muestra activo(Boolean activo) {
        this.activo = activo;
        return this;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public Muestra databaseName(String databaseName) {
        this.databaseName = databaseName;
        return this;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public String getDatabaseKey() {
        return databaseKey;
    }

    public Muestra databaseKey(String databaseKey) {
        this.databaseKey = databaseKey;
        return this;
    }

    public void setDatabaseKey(String databaseKey) {
        this.databaseKey = databaseKey;
    }

    public String getDatabaseSecret() {
        return databaseSecret;
    }

    public Muestra databaseSecret(String databaseSecret) {
        this.databaseSecret = databaseSecret;
        return this;
    }

    public void setDatabaseSecret(String databaseSecret) {
        this.databaseSecret = databaseSecret;
    }

    public Long getOcupado() {
        return ocupado;
    }

    public Muestra ocupado(Long ocupado) {
        this.ocupado = ocupado;
        return this;
    }

    public void setOcupado(Long ocupado) {
        this.ocupado = ocupado;
    }

    public Set<Registro> getRegistros() {
        return registros;
    }

    public Muestra registros(Set<Registro> registros) {
        this.registros = registros;
        return this;
    }

    public Muestra addRegistro(Registro registro) {
        this.registros.add(registro);
        registro.setMuestra(this);
        return this;
    }

    public Muestra removeRegistro(Registro registro) {
        this.registros.remove(registro);
        registro.setMuestra(null);
        return this;
    }

    public void setRegistros(Set<Registro> registros) {
        this.registros = registros;
    }

    public Formulario getFormulario() {
        return formulario;
    }

    public Muestra formulario(Formulario formulario) {
        this.formulario = formulario;
        return this;
    }

    public void setFormulario(Formulario formulario) {
        this.formulario = formulario;
    }

    public User getUser() {
        return user;
    }

    public Muestra user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Muestra)) {
            return false;
        }
        return id != null && id.equals(((Muestra) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Muestra{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", logo='" + getLogo() + "'" +
            ", logoContentType='" + getLogoContentType() + "'" +
            ", inicio='" + getInicio() + "'" +
            ", activo='" + isActivo() + "'" +
            ", ocupado=" + getOcupado() +
            "}";
    }
}
