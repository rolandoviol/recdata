package com.roviol.domain;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.roviol.event.FormularioListener;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;

/**
 * A Formulario.
 */
@Entity
@Table(name = "formulario")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@EntityListeners(FormularioListener.class)
public class Formulario implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @Column(name = "descripcion")
    private String descripcion;

    @Column(name = "revision")
    private String revision;

    @Column(name = "creado")
    private Instant creado;

    @Column(name = "modificado")
    private Instant modificado;

    @OneToMany(mappedBy = "formulario", fetch = FetchType.EAGER)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Campo> campos = new HashSet<>();

    @OneToMany(mappedBy = "formulario", fetch = FetchType.LAZY)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<Muestra> muestras = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties("formularios")
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public Formulario nombre(String nombre) {
        this.nombre = nombre;
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Formulario descripcion(String descripcion) {
        this.descripcion = descripcion;
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRevision() {
        return revision;
    }

    public Formulario revision(String revision) {
        this.revision = revision;
        return this;
    }

    public void setRevision(String revision) {
        this.revision = revision;
    }

    public Instant getCreado() {
        return creado;
    }

    public Formulario creado(Instant creado) {
        this.creado = creado;
        return this;
    }

    public void setCreado(Instant creado) {
        this.creado = creado;
    }

    public Instant getModificado() {
        return modificado;
    }

    public Formulario modificado(Instant modificado) {
        this.modificado = modificado;
        return this;
    }

    public void setModificado(Instant modificado) {
        this.modificado = modificado;
    }

    public Set<Campo> getCampos() {
        return campos;
    }

    public Formulario campos(Set<Campo> campos) {
        this.campos = campos;
        return this;
    }

    public Formulario addCampo(Campo campo) {
        this.campos.add(campo);
        campo.setFormulario(this);
        return this;
    }

    public Formulario removeCampo(Campo campo) {
        this.campos.remove(campo);
        campo.setFormulario(null);
        return this;
    }

    public void setCampos(Set<Campo> campos) {
        this.campos = campos;
    }

    public Set<Muestra> getMuestras() {
        return muestras;
    }

    public Formulario muestras(Set<Muestra> muestras) {
        this.muestras = muestras;
        return this;
    }

    public Formulario addMuestra(Muestra muestra) {
        this.muestras.add(muestra);
        muestra.setFormulario(this);
        return this;
    }

    public Formulario removeMuestra(Muestra muestra) {
        this.muestras.remove(muestra);
        muestra.setFormulario(null);
        return this;
    }

    public void setMuestras(Set<Muestra> muestras) {
        this.muestras = muestras;
    }

    public User getUser() {
        return user;
    }

    public Formulario user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Formulario)) {
            return false;
        }
        return id != null && id.equals(((Formulario) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "Formulario{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            ", revision='" + getRevision() + "'" +
            ", creado='" + getCreado() + "'" +
            ", modificado='" + getModificado() + "'" +
            "}";
    }
}
