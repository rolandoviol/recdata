package com.roviol.domain.enumeration;

/**
 * The TIPO enumeration.
 */
public enum TIPO {
    Text, MultilineText, Email, Password, Phone, Number, Decimal, Switch, Stepper, Slider, Picker, SegmentedEditor, List, DatePicker, TimePicker, AutoCompleteInline, Label
}
