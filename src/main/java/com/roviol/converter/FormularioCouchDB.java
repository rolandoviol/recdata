package com.roviol.converter;

import com.fasterxml.jackson.annotation.*;
import com.roviol.web.rest.vm.flutter.DynamicForm;
import com.roviol.web.rest.vm.nanosql.ModelNanoSQL;
import com.roviol.web.rest.vm.nativescript.RadDataForm;

import java.io.Serializable;
import java.util.Map;

public class FormularioCouchDB implements Serializable {

    //@JsonProperty("_id")
    private String id="form";

    private String _id="form";

    private String nombre;

    private String descripcion;

    private String _rev;

    @JsonInclude
    private RadDataForm radDataForm;

    @JsonInclude
    private String tipo = "form";

    @JsonInclude
    public boolean update = false;

    private String nombreMuestra;

    @JsonInclude
    private ModelNanoSQL modelNanoSQL;

    private Map newData;

    @JsonProperty("dynamic")
    @JsonInclude
    private DynamicForm dynamicForm;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = "form";
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String get_rev() {
        return _rev;
    }

    public void set_rev(String _rev) {
        this._rev = _rev;
    }

    public RadDataForm getRadDataForm() {
        return radDataForm;
    }

    public void setRadDataForm(RadDataForm radDataForm) {
        this.radDataForm = radDataForm;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getNombreMuestra() {
        return nombreMuestra;
    }

    public void setNombreMuestra(String nombreMuestra) {
        this.nombreMuestra = nombreMuestra;
    }

    public ModelNanoSQL getModelNanoSQL() {
        return modelNanoSQL;
    }

    public void setModelNanoSQL(ModelNanoSQL modelNanoSQL) {
        this.modelNanoSQL = modelNanoSQL;
    }

    public Map getNewData() {
        return newData;
    }

    public void setNewData(Map newData) {
        this.newData = newData;
    }

    public DynamicForm getDynamicForm() {
        return dynamicForm;
    }

    public void setDynamicForm(DynamicForm dynamicForm) {
        this.dynamicForm = dynamicForm;
    }

    @Override
    public String toString() {
        return "FormularioCouchDB{" +
            "id='" + id + '\'' +
            ", _id='" + _id + '\'' +
            ", nombre='" + nombre + '\'' +
            ", descripcion='" + descripcion + '\'' +
            ", _rev='" + _rev + '\'' +
            ", tipo='" + tipo + '\'' +
            ", nombreMuestra='" + nombreMuestra + '\'' +
            '}';
    }
}
