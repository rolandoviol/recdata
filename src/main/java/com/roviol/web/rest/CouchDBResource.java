package com.roviol.web.rest;

import com.roviol.security.AuthoritiesConstants;
import com.roviol.security.SecurityUtils;
import com.roviol.service.CouchDBService;
import com.roviol.web.rest.errors.BadRequestAlertException;
import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.net.MalformedURLException;
import java.util.HashMap;

/**
 * REST controller for CouchDB.
 */
@RestController
@RequestMapping("/api")
public class CouchDBResource {

    private final Logger log = LoggerFactory.getLogger(CouchDBResource.class);

    private static final String ENTITY_NAME = "couchdb";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;


    private final CouchDBService couchDBService;

    public CouchDBResource(CouchDBService couchDBService) {
        this.couchDBService = couchDBService;
    }

    /**
     * {@code GET  /couchdb} : sincroniza couchdb.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)}.
     */
    @GetMapping("/couchdb")
    public ResponseEntity<HashMap> getVerifica() throws MalformedURLException {
        log.debug("REST request");

        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            log.debug("No user passed in, using current user: {}", SecurityUtils.getCurrentUserLogin());
            throw new BadRequestAlertException("No permitido", ENTITY_NAME, "seguridad");
        }

        this.couchDBService.conexion();
        HashMap result = new HashMap();
        result.put("status", "ok");

        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, "ok")).body(result);
    }


}
