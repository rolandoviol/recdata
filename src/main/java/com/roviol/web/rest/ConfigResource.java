package com.roviol.web.rest;

import com.roviol.service.util.ZXingHelper;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * REST controller for Config.
 */
@RestController
@RequestMapping("/api")
public class ConfigResource {

    private final Logger log = LoggerFactory.getLogger(ConfigResource.class);

    private static final String ENTITY_NAME = "config";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    /**
     * {@code GET  /server} : server config.
     *
     * @return the {image} with status {@code 200 (OK)}.
     */
   // @GetMapping("/server")
    @RequestMapping(value = "/server", method = RequestMethod.GET,
        produces = "image/png")
    public @ResponseBody
    byte[] barcode(HttpServletRequest request, HttpServletResponse response) throws Exception {
        return ZXingHelper.getBarCodeImage(request.getLocalAddr(), 200, 200);
    }
}
