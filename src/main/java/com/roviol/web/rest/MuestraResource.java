package com.roviol.web.rest;

import com.roviol.domain.Muestra;
import com.roviol.domain.User;
import com.roviol.repository.MuestraRepository;
import com.roviol.repository.UserRepository;
import com.roviol.security.AuthoritiesConstants;
import com.roviol.security.SecurityUtils;
import com.roviol.service.CouchDBService;
import com.roviol.service.util.IconsUtil;
import com.roviol.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.roviol.domain.Muestra}.
 */
@RestController
@RequestMapping("/api")
public class MuestraResource {

    private final Logger log = LoggerFactory.getLogger(MuestraResource.class);

    private static final String ENTITY_NAME = "muestra";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MuestraRepository muestraRepository;
    private final UserRepository userRepository;
    private final CouchDBService couchDBService;

    public MuestraResource(MuestraRepository muestraRepository, UserRepository userRepository, CouchDBService couchDBService) {
        this.muestraRepository = muestraRepository;
        this.userRepository = userRepository;
        this.couchDBService = couchDBService;
    }

    /**
     * {@code POST  /muestras} : Create a new muestra.
     *
     * @param muestra the muestra to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new muestra, or with status {@code 400 (Bad Request)} if the muestra has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/muestras")
    public ResponseEntity<Muestra> createMuestra(@Valid @RequestBody Muestra muestra) throws URISyntaxException {
        log.debug("REST request to save Muestra : {}", muestra);
        if (muestra.getId() != null) {
            throw new BadRequestAlertException("A new muestra cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            log.debug("No user passed in, using current user: {}", SecurityUtils.getCurrentUserLogin());
            if (SecurityUtils.getCurrentUserLogin().isPresent()) {
                Optional<String> usuarioActual = SecurityUtils.getCurrentUserLogin();
                if (usuarioActual.isPresent()) {
                    Optional<User> userActual = userRepository.findOneByLogin(usuarioActual.get());
                    if (userActual.isPresent()) {
                        muestra.setUser(userActual.get());
                    }
                }
            }

        }
        if (muestra.getLogo() == null ) {
            //byte[] icono = IconsUtil.genPng(muestra.getNombre());
            byte[] icono = IconsUtil.genPngShape(64);
            muestra.setLogo(icono);
            muestra.setLogoContentType("image/png");
        }
        Muestra result = muestraRepository.save(muestra);
        couchDBService.createMuestra(muestra);
        return ResponseEntity.created(new URI("/api/muestras/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /muestras} : Updates an existing muestra.
     *
     * @param muestra the muestra to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated muestra,
     * or with status {@code 400 (Bad Request)} if the muestra is not valid,
     * or with status {@code 500 (Internal Server Error)} if the muestra couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/muestras")
    public ResponseEntity<Muestra> updateMuestra(@Valid @RequestBody Muestra muestra) throws URISyntaxException {
        log.debug("REST request to update Muestra : {}", muestra);
        if (muestra.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Optional<Muestra> muestraver;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            muestraver = muestraRepository.findById(muestra.getId());
        } else {
            muestraver = muestraRepository.findByUserIsCurrentUserAndId(muestra.getId());
        }

        if (muestraver.isPresent()){

            if (muestra.getLogo() == null ) {
                //byte[] icono = IconsUtil.genPng(muestra.getNombre());
                byte[] icono = IconsUtil.genPngShape(64);
                muestra.setLogo(icono);
                muestra.setLogoContentType("image/png");
            }
            Muestra result = muestraRepository.save(muestra);
            couchDBService.createMuestra(muestra);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, muestra.getId().toString()))
                .body(result);
        }else {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

    /**
     * {@code GET  /muestras} : get all the muestras.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of muestras in body.
     */
    @GetMapping("/muestras")
    public ResponseEntity<List<Muestra>> getAllMuestras(Pageable pageable) {
        log.debug("REST request to get a page of Muestras");
        Page<Muestra> page;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            page = muestraRepository.findAll(pageable);
        } else {
            page = muestraRepository.findByUserIsCurrentUser(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /muestras/:id} : get the "id" muestra.
     *
     * @param id the id of the muestra to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the muestra, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/muestras/{id}")
    public ResponseEntity<Muestra> getMuestra(@PathVariable Long id) {
        log.debug("REST request to get Muestra : {}", id);
        Optional<Muestra> muestra;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            muestra = muestraRepository.findById(id);
        } else {
            muestra = muestraRepository.findByUserIsCurrentUserAndId(id);
        }
        return ResponseUtil.wrapOrNotFound(muestra);
    }

    /**
     * {@code DELETE  /muestras/:id} : delete the "id" muestra.
     *
     * @param id the id of the muestra to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/muestras/{id}")
    public ResponseEntity<Void> deleteMuestra(@PathVariable Long id) {
        log.debug("REST request to delete Muestra : {}", id);

        Optional<Muestra> muestra;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            muestra = muestraRepository.findById(id);
        } else {
            muestra = muestraRepository.findByUserIsCurrentUserAndId(id);
        }
        if (muestra.isPresent()) {
            muestraRepository.deleteById(id);
            return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
        }else{
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }


    /**
     * {@code PATCH  /muestras/:id} : refresh muestra at CouchDB.
     *
     * @param id the id of the muestra to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)}
     * and with body the muestra, or with status {@code 404 (Not Found)}.
     */
    @PatchMapping("/muestras/{id}")
    public ResponseEntity<Muestra> refreshMuestra(@PathVariable Long id) {
        log.debug("REST request to patch Muestra : {}", id);
        Optional<Muestra> muestra;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            muestra = muestraRepository.findById(id);
        } else {
            muestra = muestraRepository.findByUserIsCurrentUserAndId(id);
        }
        if (muestra.isPresent()){
            couchDBService.createMuestra(muestra.get());
        }
        return ResponseUtil.wrapOrNotFound(muestra);
    }
}
