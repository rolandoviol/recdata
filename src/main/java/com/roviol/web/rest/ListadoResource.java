package com.roviol.web.rest;

import com.roviol.domain.Listado;
import com.roviol.domain.User;
import com.roviol.repository.ListadoRepository;
import com.roviol.repository.UserRepository;
import com.roviol.security.AuthoritiesConstants;
import com.roviol.security.SecurityUtils;
import com.roviol.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.roviol.domain.Listado}.
 */
@RestController
@RequestMapping("/api")
public class ListadoResource {

    private final Logger log = LoggerFactory.getLogger(ListadoResource.class);

    private static final String ENTITY_NAME = "listado";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ListadoRepository listadoRepository;
    private final UserRepository userRepository;

    public ListadoResource(ListadoRepository listadoRepository, UserRepository userRepository) {
        this.listadoRepository = listadoRepository;
        this.userRepository = userRepository;
    }

    /**
     * {@code POST  /listados} : Create a new listado.
     *
     * @param listado the listado to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new listado, or with status {@code 400 (Bad Request)} if the listado has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/listados")
    public ResponseEntity<Listado> createListado(@Valid @RequestBody Listado listado) throws URISyntaxException {
        log.debug("REST request to save Listado : {}", listado);
        if (listado.getId() != null) {
            throw new BadRequestAlertException("A new listado cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            log.debug("No user passed in, using current user: {}", SecurityUtils.getCurrentUserLogin());
            if (SecurityUtils.getCurrentUserLogin().isPresent()) {
                Optional<String> usuarioActual = SecurityUtils.getCurrentUserLogin();
                if (usuarioActual.isPresent()) {
                    Optional<User> userActual = userRepository.findOneByLogin(usuarioActual.get());
                    if (userActual.isPresent()) {
                        listado.setUser(userActual.get());
                    }
                }
            }

        }
        Listado result = listadoRepository.save(listado);
        return ResponseEntity.created(new URI("/api/listados/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /listados} : Updates an existing listado.
     *
     * @param listado the listado to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated listado,
     * or with status {@code 400 (Bad Request)} if the listado is not valid,
     * or with status {@code 500 (Internal Server Error)} if the listado couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/listados")
    public ResponseEntity<Listado> updateListado(@Valid @RequestBody Listado listado) throws URISyntaxException {
        log.debug("REST request to update Listado : {}", listado);
        if (listado.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Optional<Listado> lista;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            lista = listadoRepository.findById(listado.getId());
        } else {
            lista = listadoRepository.findByUserIsCurrentUserAndId(listado.getId());
        }

        if (lista.isPresent()){
            Listado result = listadoRepository.save(listado);
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, listado.getId().toString()))
                .body(result);
        }else {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
    }

    /**
     * {@code GET  /listados} : get all the listados.
     *

     * @param pageable the pagination information.

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of listados in body.
     */
    @GetMapping("/listados")
    public ResponseEntity<List<Listado>> getAllListados(Pageable pageable) {
        log.debug("REST request to get a page of Listados");
        Page<Listado> page;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            page = listadoRepository.findAll(pageable);
        } else {
            page = listadoRepository.findByUserIsCurrentUser(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /listados/:id} : get the "id" listado.
     *
     * @param id the id of the listado to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the listado, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/listados/{id}")
    public ResponseEntity<Listado> getListado(@PathVariable Long id) {
        log.debug("REST request to get Listado : {}", id);
        Optional<Listado> listado;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            listado = listadoRepository.findById(id);
        } else {
            listado = listadoRepository.findByUserIsCurrentUserAndId(id);
        }
        return ResponseUtil.wrapOrNotFound(listado);
    }

    /**
     * {@code DELETE  /listados/:id} : delete the "id" listado.
     *
     * @param id the id of the listado to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/listados/{id}")
    public ResponseEntity<Void> deleteListado(@PathVariable Long id) {
        log.debug("REST request to delete Listado : {}", id);

        Optional<Listado> lista;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            lista = listadoRepository.findById(id);
        } else {
            lista = listadoRepository.findByUserIsCurrentUserAndId(id);
        }

        if (lista.isPresent()) {
            listadoRepository.deleteById(id);
            return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
        }else{
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

    }
}
