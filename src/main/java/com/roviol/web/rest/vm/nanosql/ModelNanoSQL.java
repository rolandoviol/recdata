package com.roviol.web.rest.vm.nanosql;

import java.util.Map;

public class ModelNanoSQL {
    private String name;
    private Map model ;

    public ModelNanoSQL(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Map getModel() {
        return model;
    }

    public void setModel(Map model) {
        this.model = model;
    }

}
