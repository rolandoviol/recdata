package com.roviol.web.rest.vm.flutter;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.Set;

public class DynamicComponent {

    @JsonProperty("@name")
    String name;
    String id;
    String label;

    @JsonIgnore
    int index;

    @JsonIgnore
    String editor;

    @JsonIgnore
    Boolean ignore;
    @JsonIgnore
    Boolean readOnly;
    @JsonIgnore
    Boolean required;

    private Set<DynamicComponent> children = new HashSet<>();

    public DynamicComponent() {
        this.name = "formGroup";
        this.id = "formGroup1";
        this.label = "Form section 1";
    }

    public DynamicComponent(String name, String id, String label) {
        this.name = name;
        this.id = id;
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Boolean getIgnore() {
        return ignore;
    }

    public void setIgnore(Boolean ignore) {
        this.ignore = ignore;
    }

    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }

    public Set<DynamicComponent> getChildren() {
        if (this.children.isEmpty()){
            return null;
        }
        return children;
    }

    public void setChildren(Set<DynamicComponent> children) {
        this.children = children;
    }

    public DynamicComponent addChildren(DynamicComponent dynamicComponent) {
        this.children.add(dynamicComponent);
        return this;
    }
}
