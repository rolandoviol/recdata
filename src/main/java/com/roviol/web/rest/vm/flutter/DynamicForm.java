package com.roviol.web.rest.vm.flutter;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashSet;
import java.util.Set;

public class DynamicForm {

    @JsonProperty("@name")
    String name;
    String id;
    private Set<DynamicComponent> children = new HashSet<>();

    public DynamicForm() {
        this.name = "form";
        this.id = "form1";

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Set<com.roviol.web.rest.vm.flutter.DynamicComponent> getChildren() {
        return this.children;
    }

    public void setChildren(Set<com.roviol.web.rest.vm.flutter.DynamicComponent> children) {
        this.children = children;
    }

    public void addChildren(DynamicComponent dynamicComponent) {
        children.add(dynamicComponent);
    }
}
