/**
 * View Models used by Spring MVC REST controllers.
 */
package com.roviol.web.rest.vm;
