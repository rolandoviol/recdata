package com.roviol.web.rest.vm.nativescript;

import java.util.HashSet;
import java.util.Set;

public class RadDataForm {
    boolean isReadOnly;
    String commitMode;
    String validationMode;
    private Set<PropertyAnnotation> propertyAnnotations = new HashSet<>();

    public RadDataForm() {
        this.isReadOnly = false;
        this.commitMode = "Immediate";
        this.validationMode = "Immediate";
    }

    public boolean isReadOnly() {
        return isReadOnly;
    }

    public void setReadOnly(boolean readOnly) {
        isReadOnly = readOnly;
    }

    public String getCommitMode() {
        return commitMode;
    }

    public void setCommitMode(String commitMode) {
        this.commitMode = commitMode;
    }

    public String getValidationMode() {
        return validationMode;
    }

    public void setValidationMode(String validationMode) {
        this.validationMode = validationMode;
    }

    public Set<PropertyAnnotation> getPropertyAnnotations() {
        return propertyAnnotations;
    }

    public void setPropertyAnnotations(Set<PropertyAnnotation> propertyAnnotations) {
        this.propertyAnnotations = propertyAnnotations;
    }

    public RadDataForm addPropertyAnnotation(PropertyAnnotation propertyAnnotation) {
        this.propertyAnnotations.add(propertyAnnotation);
        return this;
    }
}
