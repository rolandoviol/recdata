package com.roviol.web.rest.vm.nativescript;

import com.roviol.domain.Campo;

public class PropertyAnnotation {
    String name;
    String displayName;
    int index;
    String editor;
    Boolean ignore;
    Boolean readOnly;
    Boolean required;

    public PropertyAnnotation() {
        this.ignore = false;
        this.readOnly = false;
        this.required = false;
        this.editor = "Text";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public Boolean getIgnore() {
        return ignore;
    }

    public void setIgnore(Boolean ignore) {
        this.ignore = ignore;
    }

    public Boolean getReadOnly() {
        return readOnly;
    }

    public void setReadOnly(Boolean readOnly) {
        this.readOnly = readOnly;
    }

    public Boolean getRequired() {
        return required;
    }

    public void setRequired(Boolean required) {
        this.required = required;
    }


    public void setCampo(Campo campo){
        this.setName(campo.getFieldname());
        this.setDisplayName(campo.getDescripcion());
        this.setRequired(campo.isRequerido());
    }
}
