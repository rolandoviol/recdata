package com.roviol.web.rest.vm.nativescript;

import com.roviol.domain.Muestra;
import com.roviol.web.rest.vm.nanosql.ModelNanoSQL;

import java.time.LocalDate;

public class Muestreo {
    private String muestra;
    private String titulo;
    private String descripcion;
    private LocalDate inicio;
    private byte[] logo;
    private String logoContentType;
    private RadDataForm radDataForm;
    private ModelNanoSQL modelNanoSQL;

    public Muestreo() {
    }

    public String getMuestra() {
        return muestra;
    }

    public void setMuestra(String muestra) {
        this.muestra = muestra;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LocalDate getInicio() {
        return inicio;
    }

    public void setInicio(LocalDate inicio) {
        this.inicio = inicio;
    }

    public byte[] getLogo() {
        return logo;
    }

    public void setLogo(byte[] logo) {
        this.logo = logo;
    }

    public String getLogoContentType() {
        return logoContentType;
    }

    public void setLogoContentType(String logoContentType) {
        this.logoContentType = logoContentType;
    }

    public RadDataForm getRadDataForm() {
        return radDataForm;
    }

    public void setRadDataForm(RadDataForm radDataForm) {
        this.radDataForm = radDataForm;
    }

    public ModelNanoSQL getModelNanoSQL() {
        return modelNanoSQL;
    }

    public void setModelNanoSQL(ModelNanoSQL modelNanoSQL) {
        this.modelNanoSQL = modelNanoSQL;
    }

    public void setMuestra(Muestra muestra){
        this.setMuestra(muestra.getId().toString());
        this.setTitulo(muestra.getNombre());
        this.setDescripcion("");
        this.setLogo(muestra.getLogo());
        this.setInicio(muestra.getInicio());
        this.setLogoContentType(muestra.getLogoContentType());
    }

}
