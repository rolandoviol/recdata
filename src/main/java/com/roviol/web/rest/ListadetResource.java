package com.roviol.web.rest;

import com.roviol.domain.Listadet;
import com.roviol.domain.Listado;
import com.roviol.repository.ListadetRepository;
import com.roviol.repository.ListadoRepository;
import com.roviol.security.AuthoritiesConstants;
import com.roviol.security.SecurityUtils;
import com.roviol.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.roviol.domain.Listadet}.
 */
@RestController
@RequestMapping("/api")
public class ListadetResource {

    private final Logger log = LoggerFactory.getLogger(ListadetResource.class);

    private static final String ENTITY_NAME = "listadet";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ListadetRepository listadetRepository;
    private final ListadoRepository listadoRepository;

    public ListadetResource(ListadetRepository listadetRepository, ListadoRepository listadoRepository) {
        this.listadetRepository = listadetRepository;
        this.listadoRepository = listadoRepository;
    }

    /**
     * {@code POST  /listadets} : Create a new listadet.
     *
     * @param listadet the listadet to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new listadet, or with status {@code 400 (Bad Request)} if the listadet has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/listadets")
    public ResponseEntity<Listadet> createListadet(@Valid @RequestBody Listadet listadet) throws URISyntaxException {
        log.debug("REST request to save Listadet : {}", listadet);

        verificaListado(listadet.getListado().getId());

        if (listadet.getId() != null) {
            throw new BadRequestAlertException("A new listadet cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Listadet result = listadetRepository.save(listadet);
        return ResponseEntity.created(new URI("/api/listadets/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /listadets} : Updates an existing listadet.
     *
     * @param listadet the listadet to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated listadet,
     * or with status {@code 400 (Bad Request)} if the listadet is not valid,
     * or with status {@code 500 (Internal Server Error)} if the listadet couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/listadets")
    public ResponseEntity<Listadet> updateListadet(@Valid @RequestBody Listadet listadet) throws URISyntaxException {
        log.debug("REST request to update Listadet : {}", listadet);

        verificaListado(listadet.getListado().getId());

        if (listadet.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Listadet result = listadetRepository.save(listadet);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, listadet.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /listadets} : get all the listadets.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of listadets in body.
     */
    @GetMapping("/listadets")
    public ResponseEntity<List<Listadet>> getAllListadets(Pageable pageable) {
        log.debug("REST request to get a page of Listadets");
        Page<Listadet> page = null;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            page = listadetRepository.findAll(pageable);
        }else{
            page = listadetRepository.findByUserIsCurrentUser(pageable);
        }
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /listadets/:id} : get the "id" listadet.
     *
     * @param id the id of the listadet to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the listadet, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/listadets/{id}")
    public ResponseEntity<Listadet> getListadet(@PathVariable Long id) {
        log.debug("REST request to get Listadet : {}", id);
        Optional<Listadet> listadet = listadetRepository.findById(id);

        if (listadet.isPresent()) {
            verificaListado(listadet.get().getListado().getId());
        }

        return ResponseUtil.wrapOrNotFound(listadet);
    }

    /**
     * {@code DELETE  /listadets/:id} : delete the "id" listadet.
     *
     * @param id the id of the listadet to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/listadets/{id}")
    public ResponseEntity<Void> deleteListadet(@PathVariable Long id) {
        log.debug("REST request to delete Listadet : {}", id);
        Optional<Listadet> listadet = listadetRepository.findById(id);

        if (listadet.isPresent()) {
            verificaListado(listadet.get().getListado().getId());
        }

        listadetRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /listadets/listado/:id} : get all the listadet de un listado.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of campos in body.
     */
    @GetMapping("/listadets/listado/{id}")
    public ResponseEntity<List<Listadet>> getAllListadetsListado(Pageable pageable, @PathVariable Long id) {
        log.debug("REST request to get all Listadet Listado {}", id);
        Page<Listadet> page;
        if (!(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN))) {
            verificaListado(id);
        }
        page = listadetRepository.findAllByListado_Id(pageable, id);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    private boolean verificaListado(Long idlista){

        Optional<Listado> listado;
        if (!(SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN))) {
            listado = listadoRepository.findByUserIsCurrentUserAndId(idlista);
            if (!listado.isPresent()){
                throw new BadRequestAlertException("Listado no existe para el usuario", ENTITY_NAME, "idexists");
            }
        }
        return true;
    }
}
