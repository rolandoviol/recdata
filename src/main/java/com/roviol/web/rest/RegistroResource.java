package com.roviol.web.rest;

import com.roviol.domain.Registro;
import com.roviol.repository.RegistroRepository;
import com.roviol.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.roviol.domain.Registro}.
 */
@RestController
@RequestMapping("/api")
public class RegistroResource {

    private final Logger log = LoggerFactory.getLogger(RegistroResource.class);

    private static final String ENTITY_NAME = "registro";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegistroRepository registroRepository;

    public RegistroResource(RegistroRepository registroRepository) {
        this.registroRepository = registroRepository;
    }

    /**
     * {@code POST  /registros} : Create a new registro.
     *
     * @param registro the registro to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new registro, or with status {@code 400 (Bad Request)} if the registro has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/registros")
    public ResponseEntity<Registro> createRegistro(@Valid @RequestBody Registro registro) throws URISyntaxException {
        log.debug("REST request to save Registro : {}", registro);
        if (registro.getId() != null) {
            throw new BadRequestAlertException("A new registro cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Registro result = registroRepository.save(registro);
        return ResponseEntity.created(new URI("/api/registros/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /registros} : Updates an existing registro.
     *
     * @param registro the registro to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated registro,
     * or with status {@code 400 (Bad Request)} if the registro is not valid,
     * or with status {@code 500 (Internal Server Error)} if the registro couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/registros")
    public ResponseEntity<Registro> updateRegistro(@Valid @RequestBody Registro registro) throws URISyntaxException {
        log.debug("REST request to update Registro : {}", registro);
        if (registro.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Registro result = registroRepository.save(registro);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, registro.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /registros} : get all the registros.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of registros in body.
     */
    @GetMapping("/registros")
    public List<Registro> getAllRegistros() {
        log.debug("REST request to get all Registros");
        return registroRepository.findAll();
    }

    /**
     * {@code GET  /registros/:id} : get the "id" registro.
     *
     * @param id the id of the registro to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the registro, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/registros/{id}")
    public ResponseEntity<Registro> getRegistro(@PathVariable Long id) {
        log.debug("REST request to get Registro : {}", id);
        Optional<Registro> registro = registroRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(registro);
    }

    /**
     * {@code DELETE  /registros/:id} : delete the "id" registro.
     *
     * @param id the id of the registro to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/registros/{id}")
    public ResponseEntity<Void> deleteRegistro(@PathVariable Long id) {
        log.debug("REST request to delete Registro : {}", id);
        registroRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
