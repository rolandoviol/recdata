package com.roviol.web.rest;

import com.roviol.domain.Campo;
import com.roviol.domain.Formulario;
import com.roviol.domain.Muestra;
import com.roviol.domain.User;
import com.roviol.repository.CampoRepository;
import com.roviol.repository.FormularioRepository;
import com.roviol.repository.MuestraRepository;
import com.roviol.repository.UserRepository;
import com.roviol.security.AuthoritiesConstants;
import com.roviol.security.SecurityUtils;
import com.roviol.service.CouchDBService;
import com.roviol.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * REST controller for managing {@link com.roviol.domain.Formulario}.
 */
@RestController
@RequestMapping("/api")
public class FormularioResource {

    private final Logger log = LoggerFactory.getLogger(FormularioResource.class);

    private static final String ENTITY_NAME = "formulario";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final FormularioRepository formularioRepository;
    private final UserRepository userRepository;
    private final CampoRepository campoRepository;
    private final CouchDBService couchDBService;
    private  final MuestraRepository muestraRepository;

    public FormularioResource(FormularioRepository formularioRepository, UserRepository userRepository, CampoRepository campoRepository, CouchDBService couchDBService, MuestraRepository muestraRepository) {
        this.formularioRepository = formularioRepository;
        this.userRepository = userRepository;
        this.campoRepository = campoRepository;
        this.couchDBService = couchDBService;
        this.muestraRepository = muestraRepository;
    }

    /**
     * {@code POST  /formularios} : Create a new formulario.
     *
     * @param formulario the formulario to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new formulario, or with status {@code 400 (Bad Request)} if the formulario has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/formularios")
    public ResponseEntity<Formulario> createFormulario(@Valid @RequestBody Formulario formulario) throws URISyntaxException {
        log.debug("REST request to save Formulario : {}", formulario);
        if (formulario.getId() != null) {
            throw new BadRequestAlertException("A new formulario cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (!SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
             log.debug("No user passed in, using current user: {}", SecurityUtils.getCurrentUserLogin());
             if (SecurityUtils.getCurrentUserLogin().isPresent()) {
                 Optional<String> usuarioActual = SecurityUtils.getCurrentUserLogin();
                 if (usuarioActual.isPresent()) {
                     Optional<User> userActual = userRepository.findOneByLogin(usuarioActual.get());
                     if (userActual.isPresent()) {
                         formulario.setUser(userActual.get());
                     }
                 }
             }
        }
        Formulario result = formularioRepository.save(formulario);
        return ResponseEntity.created(new URI("/api/formularios/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /formularios} : Updates an existing formulario.
     *
     * @param formulario the formulario to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated formulario,
     * or with status {@code 400 (Bad Request)} if the formulario is not valid,
     * or with status {@code 500 (Internal Server Error)} if the formulario couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/formularios")
    public ResponseEntity<Formulario> updateFormulario(@Valid @RequestBody Formulario formulario) throws URISyntaxException {
        log.debug("REST request to update Formulario : {}", formulario);

        if (formulario.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        Optional<Formulario> form;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
           form = formularioRepository.findById(formulario.getId());
        } else {
           form = formularioRepository.findByUserIsCurrentUserAndId(formulario.getId());
        }

        Iterator<Campo> camposIter = formulario.getCampos().iterator();

        while(camposIter.hasNext()){
            Campo campoForm = camposIter.next();
            log.debug("REST request to update Campo : {}", campoForm);
            Optional<Campo> campo = null;
            if (campoForm.getId() != null){
                if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
                    campo = campoRepository.findById(campoForm.getId());
                } else {
                    if(form.isPresent()) {
                        campo = campoRepository.findByIdAAndFormulario_Id(campoForm.getId(), form.get().getId());
                    }
                }
            }

            //if (campo.isPresent()){
            //    campoRepository.save(campoForm);
            //}else{
                if (campoForm.getId() == null) {
                    campoRepository.save(campoForm);
                }else {
                    if (campo.isPresent()){
                        Campo campoFormExist = campo.get();
                        campoFormExist.setDescripcion(campoForm.getDescripcion());
                        campoFormExist.setNombreCampo(campoForm.getNombreCampo());
                        campoFormExist.setTipo(campoForm.getTipo());
                        campoFormExist.setListado(campoForm.getListado());
                        campoFormExist.setPosicion(campoForm.getPosicion());
                        campoFormExist.setRequerido(campoForm.isRequerido());
                        campoRepository.save(campoFormExist);
                    }
                }
            //}
        }


        if (form.isPresent()){
            formulario.setCreado(form.get().getCreado());
            formulario.setRevision(form.get().getRevision());
            Formulario result = formularioRepository.save(formulario);
            Set<Muestra> muestras = muestraRepository.findAllByFormulario(result);
            //Set<Muestra> muestras = result.getMuestras();
            for (Muestra muestra: muestras) {
                couchDBService.createMuestra(muestra);
                log.debug("REST couchDB update muestra : {}", muestra);
            }
            return ResponseEntity.ok()
                .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, formulario.getId().toString()))
                .body(result);
        }else {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }


    }

    /**
     * {@code GET  /formularios} : get all the formularios.
     *

     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of formularios in body.
     */
    @GetMapping("/formularios")
    public List<Formulario> getAllFormularios() {
        log.debug("REST request to get all Formularios");
        List<Formulario> formularios;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            formularios = formularioRepository.findAll();
        } else {
            formularios = formularioRepository.findByUserIsCurrentUser();
        }
        return formularios;
    }

    /**
     * {@code GET  /formularios/:id} : get the "id" formulario.
     *
     * @param id the id of the formulario to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the formulario, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/formularios/{id}")
    public ResponseEntity<Formulario> getFormulario(@PathVariable Long id) {
        log.debug("REST request to get Formulario : {}", id);
        Optional<Formulario> formulario;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            formulario = formularioRepository.findById(id);
        } else {
            formulario = formularioRepository.findByUserIsCurrentUserAndId(id);
        }
        return ResponseUtil.wrapOrNotFound(formulario);
    }

    /**
     * {@code DELETE  /formularios/:id} : delete the "id" formulario.
     *
     * @param id the id of the formulario to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/formularios/{id}")
    public ResponseEntity<Void> deleteFormulario(@PathVariable Long id) {
        log.debug("REST request to delete Formulario : {}", id);

        Optional<Formulario> form;
        if (SecurityUtils.isCurrentUserInRole(AuthoritiesConstants.ADMIN)) {
            form = formularioRepository.findById(id);
        } else {
            form = formularioRepository.findByUserIsCurrentUserAndId(id);
        }

        if (form.isPresent()) {
            Formulario formExist = form.get();
            Set<Campo> camposSet = formExist.getCampos();
            Iterator<Campo> campos = camposSet.iterator();
            while (campos.hasNext()){
                Campo campo = campos.next();
                campoRepository.delete(campo);
            }
            formularioRepository.deleteById(id);
            return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
        }else{
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

    }
}
