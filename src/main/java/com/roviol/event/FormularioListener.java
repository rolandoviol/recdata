package com.roviol.event;

import com.roviol.domain.Formulario;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.Instant;

public class FormularioListener {

    @PreUpdate
    public void beforeUpdate(final Formulario reference) {
        reference.setModificado(Instant.now());
    }

    @PrePersist
    public void beforeInsert(final Formulario reference) {
        reference.setCreado(Instant.now());
    }


}
