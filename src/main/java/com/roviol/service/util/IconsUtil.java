package com.roviol.service.util;

import com.roviol.service.CouchDBService;
import com.sshtools.icongenerator.AwesomeIcon;
import com.sshtools.icongenerator.IconBuilder;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.GeneralPath;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.text.Normalizer;
import java.util.List;
import java.util.*;
import java.util.regex.Pattern;

public final class IconsUtil {
    static Color[] colors = {
        Color.RED,
        Color.GREEN,
        Color.BLUE,
        Color.YELLOW
    };

    // create random object - reuse this as often as possible
    static Random random = new SecureRandom();
    private final static Logger log = LoggerFactory.getLogger(IconsUtil.class);

    public static IconBuilder genIcon(String texto) {

        IconBuilder builder = new IconBuilder();
        builder.width(64);
        builder.height(64);
        builder.color(randomColor());
        builder.textColor(IconBuilder.AUTO_TEXT_COLOR);
        builder.text(toSlug(texto).substring(0,2));

        List<AwesomeIcon> iconos = new ArrayList<AwesomeIcon>(Arrays.asList(AwesomeIcon.values()));
        builder.icon(iconos.get(RandomUtils.nextInt(0, iconos.size())));

        builder.roundRect(24);

        return builder;
    }

    public static byte[] genPng(String text){
        IconBuilder icon = genIcon(text);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(icon.build(BufferedImage.class), "png", baos);
        } catch (IOException e) {
            log.debug("genPng {}", e.getMessage());
        }

        return baos.toByteArray();

    }

    public static int randomColor(){

        // create a big random number - maximum is ffffff (hex) = 16777215 (dez)
        int nextInt = random.nextInt(0xffffff + 1);

        return nextInt;
    }

    public static Color randomColorB(){
        float r = random.nextFloat() / 2f + 0.5f;
        float g = random.nextFloat() / 2f + 0.5f;
        float b = random.nextFloat() / 2f + 0.5f;
        Color color = new Color(r, g, b);
        return color;
    }

    public static Color randomColorC(){
        float r = random.nextFloat();
        float g = random.nextFloat();
        float b = random.nextFloat();
        Color color = new Color(r, g, b);
        return color;
    }

    private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");

    public static String toSlug(String input) {
        String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        return slug.toLowerCase(Locale.ENGLISH);
    }


    public static BufferedImage getColoredShapeImage(
        int size, Shape shape, Shape shape2, Color color, Color color2) {
        BufferedImage bi = new BufferedImage(
            size, size, BufferedImage.TYPE_INT_ARGB);

        Graphics2D g = bi.createGraphics();
        g.setRenderingHint(
            RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);
        g.setColor(color);
        g.fill(shape);
        g.setColor(color2);
        int alto = shape2.getBounds().height;
        int ancho = shape2.getBounds().width;
        g.translate((size/2) - (ancho/2), (size/2) - (alto/2));
        g.fill(shape2);
        g.dispose();

        return bi;
    }

    public static Shape getPointedShape(int points, int radius) {
        double angle = Math.PI * 2 / points;

        GeneralPath p = new GeneralPath();
        for (int ii = 0; ii < points; ii++) {
            double a = points % 2 == 0 ? angle * ii : (angle * ii) - (Math.PI / 2);

            double x = (Math.cos(a) * radius) + radius;
            double y = (Math.sin(a) * radius) + radius;
            if (ii == 0) {
                p.moveTo(x, y);
            } else {
                p.lineTo(x, y);
            }
        }
        p.closePath();

        return p;
    }

    public static byte[] genPngShape(int s ){
        Color c = randomColorC();
        Color c2 = randomColorC();
        int nPoints = RandomUtils.nextInt(3, 9);
        int nPoints2 = RandomUtils.nextInt(3, 9);
        nPoints = ( nPoints % 2 == 0 ) ? nPoints + 1 : nPoints;
        nPoints2 = ( nPoints2 % 2 == 1 ) ? nPoints2 + 1 : nPoints2;
        Shape sh = getPointedShape(nPoints, s);
        Shape sh2 = getPointedShape(nPoints2, s/2);
        BufferedImage i = getColoredShapeImage((2 * s) + 2, sh, sh2, c, c2);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(i, "png", baos);
        } catch (IOException e) {
            log.debug("genPng {}", e.getMessage());
        }

        return baos.toByteArray();

    }

}
