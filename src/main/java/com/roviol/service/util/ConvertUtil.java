package com.roviol.service.util;

import com.roviol.domain.enumeration.TIPO;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;
import java.util.regex.Pattern;

public class ConvertUtil {
    private static final Pattern NONLATIN = Pattern.compile("[^\\w-]");
    private static final Pattern WHITESPACE = Pattern.compile("[\\s]");

    public static String toSlug(String input) {
        String nowhitespace = WHITESPACE.matcher(input).replaceAll("-");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        return slug.toLowerCase(Locale.ENGLISH);
    }


    public static String toField(String input) {
        String nowhitespace = WHITESPACE.matcher(input).replaceAll("");
        String normalized = Normalizer.normalize(nowhitespace, Normalizer.Form.NFD);
        String slug = NONLATIN.matcher(normalized).replaceAll("");
        return slug.toLowerCase(Locale.ENGLISH);
    }

    public static String tipoToDatatype(TIPO tipo){
        ArrayList<TIPO> tipoNumber = new ArrayList<TIPO>(Arrays.asList(
            TIPO.Number,
            TIPO.Decimal
        ));
        ArrayList<TIPO> tipoString = new ArrayList<TIPO>(Arrays.asList(
            TIPO.Text,
            TIPO.MultilineText,
            TIPO.Label,
            TIPO.Email,
            TIPO.Password,
            TIPO.Phone
        ));
        if (tipoNumber.contains(tipo)){
            return "number";
        }else if(tipoString.contains(tipo)){
            return "string";
        }else{
            return "any";
        }
    }

}
