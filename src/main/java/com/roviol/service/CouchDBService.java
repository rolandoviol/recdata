package com.roviol.service;

import com.cloudant.client.api.ClientBuilder;
import com.cloudant.client.api.CloudantClient;
import com.cloudant.client.api.Database;
import com.cloudant.client.org.lightcouch.NoDocumentException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.roviol.config.CouchDBConfiguration;
import com.roviol.converter.FormularioCouchDB;
import com.roviol.domain.Formulario;
import com.roviol.domain.Muestra;
import com.roviol.domain.Perfil;
import com.roviol.domain.User;
import com.roviol.repository.*;
import com.roviol.web.rest.vm.flutter.DynamicForm;
import com.roviol.web.rest.vm.nanosql.ModelNanoSQL;
import com.roviol.web.rest.vm.nativescript.RadDataForm;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

@Configuration
@Service
public class CouchDBService {

    @Autowired
    CouchDBConfiguration couchDBConfiguration;

    private CloudantClient authenticatedClient;
    private final Logger log = LoggerFactory.getLogger(CouchDBService.class);
    private final MuestraRepository muestraRepository;
    private final FormularioRepository formularioRepository;
    private final SchemaFormService schemaFormService;
    private final UserRepository userRepository;
    private final PerfilRepository perfilRepository;

    public CouchDBService(MuestraRepository muestraRepository,
                          FormularioRepository formularioRepository,
                          CouchDBConfiguration couchDBConfiguration,
                          SchemaFormService schemaFormService,
                          UserRepository userRepository,
                          PerfilRepository perfilRepository) {
        this.muestraRepository = muestraRepository;
        this.formularioRepository = formularioRepository;
        this.couchDBConfiguration = couchDBConfiguration;
        this.schemaFormService = schemaFormService;
        this.userRepository = userRepository;
        this.perfilRepository = perfilRepository;
    }

    @PostConstruct
    public void conexion() throws MalformedURLException {
       log.debug("Conectando a {}", this.couchDBConfiguration.getUrl());

        authenticatedClient = ClientBuilder.url(new URL(this.couchDBConfiguration.getUrl()))
            .username(this.couchDBConfiguration.getUser())
            .password(this.couchDBConfiguration.getPassword())
            .build();

        log.debug("Base de datos _users");
        Database dbUsers = authenticatedClient.database("_users", true);

        List<User> usuarios = userRepository.findAllByActivatedTrue();

        for (User usuario: usuarios){
            Optional<Perfil> perfil = perfilRepository.findAllByUser(usuario);
            if (!(perfil.isPresent())){
                Perfil perfilnuevo = new Perfil();
                perfilnuevo.setUser(usuario);
                String key = RandomStringUtils.randomAlphabetic(15);
                perfilnuevo.setSecret(key);
                perfilRepository.save(perfilnuevo);
                perfil = perfilRepository.findAllByUser(usuario);
            }
            log.debug("{}", usuario.getLogin());
            if (perfil.isPresent()) {
                this.createUser(usuario.getLogin(), perfil.get().getSecret());
            }else{
                log.info("Perfil no presenta al crear usuario {}", usuario.getLogin());
            }
        }

       List<Muestra> muestras = muestraRepository.findAll();
       for (Muestra muestra: muestras){
           this.createMuestra(muestra);
       }
   }

    private boolean createUser(String login, String password) {
        Database usuariosdb = authenticatedClient.database("_users", true);
        log.debug("Usuario Base de datos {}", login);
        Map<String, Object> map;
        try {
            map = usuariosdb.find(Map.class, "org.couchdb.user:" + login);
            map.put("password", password);
            usuariosdb.update(map);
        }catch (NoDocumentException e){
            map = new HashMap<String, Object>();
            map.put("_id", "org.couchdb.user:" + login);
            map.put("name", login);
            map.put("password", password);
            map.put("type", "user");
            map.put("roles", new ArrayList<String>());
            map.put("admin", new ArrayList<String>());
            log.debug("Usuario {}", map);
            usuariosdb.save(map);
        }
        return true;
    }

    public boolean createMuestra(Muestra muestra){

        if (muestra.getDatabaseName() == null || muestra.getDatabaseName().equals("")) {
            //muestra.setDatabaseName(this.couchDBConfiguration.getPrefijo_db() + muestra.getId());
            String key = RandomStringUtils.randomAlphabetic(5).toLowerCase();
            muestra.setDatabaseName(this.couchDBConfiguration.getPrefijo_db() + muestra.getId() + "_" + key);
        }
        if (muestra.getDatabaseKey() == null || muestra.getDatabaseKey().equals("") ) {
            String key = RandomStringUtils.randomAlphabetic(10);
            muestra.setDatabaseKey(key);
        }
        if (muestra.getDatabaseSecret() == null || muestra.getDatabaseSecret().equals("")) {
            String key = RandomStringUtils.randomAlphabetic(15);
            muestra.setDatabaseSecret(key);
        }

        log.debug("{}", muestra.getDatabaseName());
        Database muestradb = authenticatedClient.database(muestra.getDatabaseName(), true);
        long ocupado = muestradb.info().getDiskSize();
        log.debug("Size {}", ocupado);
        muestra.setOcupado(ocupado);

        boolean creado = this.createUserForDB(muestra);

        if (creado){
            muestraRepository.save(muestra);
        }

        this.createFormulario(muestra);
        return true;
    }


    public boolean createFormulario(Muestra muestra) {
        Database muestradb = authenticatedClient.database(muestra.getDatabaseName(), false);
        log.debug("Muestra CouchDB {}", muestra);

        Formulario formulario = muestra.getFormulario();
        log.debug("Formulario crear {}", formulario);

        //Existe el formulario en la metadata
        if (formulario!=null) {

            //Formulario en la couchDB
            FormularioCouchDB formularioCouchDBRemoto;

            try {
                formularioCouchDBRemoto = muestradb.find(FormularioCouchDB.class,"form");
            }catch (NoDocumentException e){
                formularioCouchDBRemoto = new FormularioCouchDB();
            }

            formularioCouchDBRemoto.setNombre(formulario.getNombre());
            formularioCouchDBRemoto.setDescripcion(formulario.getDescripcion());
            formularioCouchDBRemoto.setNombreMuestra(muestra.getNombre());

            RadDataForm radDataForm = schemaFormService.formularioToRaddataform(formulario);
            ModelNanoSQL modelNanoSQL = schemaFormService.formularioToModelNanoSQL(muestra.getDatabaseName(), formulario);
            DynamicForm dynamicForm = schemaFormService.formularioToDynamicform(formulario);
            Map newData = schemaFormService.formularioToNewData(formulario);

            formularioCouchDBRemoto.setRadDataForm(radDataForm);
            formularioCouchDBRemoto.setModelNanoSQL(modelNanoSQL);
            formularioCouchDBRemoto.setNewData(newData);

            formularioCouchDBRemoto.setDynamicForm(dynamicForm);

            log.debug("Formulario Remoto CouchDB {}", formularioCouchDBRemoto);

            //formularioCouchDBRemoto.set_rev(null);
            //Existe el formulario en la couchDB

            try {
                ObjectMapper Obj = new ObjectMapper();
                String jsonInString = Obj.writeValueAsString(formularioCouchDBRemoto);
                JsonParser jsonParser = new JsonParser();
                JsonObject mJSONObject = jsonParser.parse(jsonInString).getAsJsonObject();

                if (formularioCouchDBRemoto.get_rev()!=null){
                    muestradb.update(mJSONObject);
                }else{
                    muestradb.save(mJSONObject);
                }
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

            //this.syncFormularioRev(muestradb, formularioCouchDBRemoto, formulario);

        }
        return true;
    }

    public void syncFormularioRev(Database muestradb, FormularioCouchDB formularioCouchDB, Formulario formulario){

        try {
            FormularioCouchDB formularioCouchDBver = muestradb.find(FormularioCouchDB.class, formularioCouchDB.getId());
            //formulario.setRevision(formularioCouchDBver.getRevision());
        }catch (NoDocumentException e){
            log.debug("formularioCouchDB {}", formularioCouchDB);
        }
        formularioRepository.save(formulario);
        log.debug("formulario {}", formulario);

    }

    public boolean createUserForDB(Muestra muestra){

        String database = muestra.getDatabaseName();
        String accesskey = muestra.getDatabaseKey();
        String secretkey = muestra.getDatabaseSecret();

        Database usuariosdb = authenticatedClient.database("_users", false);
        Database muestradb = authenticatedClient.database(database, false);

        log.debug("Usuario Base de datos {}", accesskey);
        Map<String, Object> map;

        try {
            map = usuariosdb.find(Map.class, "org.couchdb.user:" + accesskey);
        }catch (NoDocumentException e){
            map = new HashMap<String, Object>();
            map.put("_id", "org.couchdb.user:" + accesskey);
            map.put("name", accesskey);
            map.put("password", secretkey);
            map.put("type", "user");
            map.put("roles", new ArrayList<String>());
            map.put("admin", new ArrayList<String>());
            log.debug("Usuario {}", map);
            usuariosdb.save(map);
        }

        Map<String, Object> security;
        try {
            security = muestradb.find(Map.class, "_security" );
        }catch (NoDocumentException e){
            security = new HashMap<String, Object>();
        }

        HashMap<String, Object> members = new HashMap<String, Object>();
        ArrayList<String> names = new ArrayList<String>();
        names.add(accesskey);
        names.add(muestra.getUser().getLogin());
        members.put("names", names);
        security.put("members", members);
        security.put("_id", "_security");
        security.put("_rev", "0");
        log.debug("Usuarios {}", security);
        muestradb.update(security);

        return true;
    }

}
