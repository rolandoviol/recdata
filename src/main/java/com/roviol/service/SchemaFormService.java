package com.roviol.service;

import com.roviol.domain.Campo;
import com.roviol.domain.Formulario;
import com.roviol.domain.Muestra;
import com.roviol.domain.enumeration.TIPO;
import com.roviol.repository.CampoRepository;
import com.roviol.repository.MuestraRepository;
import com.roviol.service.util.ConvertUtil;
import com.roviol.web.rest.vm.flutter.DynamicComponent;
import com.roviol.web.rest.vm.flutter.DynamicForm;
import com.roviol.web.rest.vm.nanosql.ModelNanoSQL;
import com.roviol.web.rest.vm.nativescript.Muestreo;
import com.roviol.web.rest.vm.nativescript.PropertyAnnotation;
import com.roviol.web.rest.vm.nativescript.RadDataForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SchemaFormService {

    private final Logger log = LoggerFactory.getLogger(SchemaFormService.class);
    private final MuestraRepository muestraRepository;
    private final CampoRepository campoRepository;

    public SchemaFormService(MuestraRepository muestraRepository, CampoRepository campoRepository) {
        this.muestraRepository = muestraRepository;
        this.campoRepository = campoRepository;
    }

    public Optional<Muestreo> muestraToMuestreo(Optional<Muestra> muestra){

        Optional<Muestreo> muestreo = Optional.of(new Muestreo());
        RadDataForm radDataForm = new RadDataForm();

        if (muestra.isPresent()) {

            log.debug("REST request to get Muestra : {}", muestra.get());
            muestreo.get().setMuestra(muestra.get());
            log.debug("formulario id {}", muestra.get().getFormulario().getId());

            Formulario formulario = muestra.get().getFormulario();

            radDataForm = formularioToRaddataform(formulario);

        }
        muestreo.get().setRadDataForm(radDataForm);

        return muestreo;
    }

    public RadDataForm formularioToRaddataform(Formulario formulario){

        RadDataForm radDataForm = new RadDataForm();

        //Buscar los campos del formulario
        Campo campoex = new Campo();
        campoex.setFormulario(formulario);
        //Example<Campo> example = Example.of(campoex);
        //List<Campo> campos = campoRepository.findAll(example);
        List<Campo> campos = campoRepository.findAllByFormulario_IdOrderByPosicion(formulario.getId());

        PropertyAnnotation pid = new PropertyAnnotation();
        pid.setName("couchdb_id");
        pid.setIndex(0);
        pid.setIgnore(true);
        radDataForm.addPropertyAnnotation(pid);

        PropertyAnnotation prev = new PropertyAnnotation();
        prev.setName("couchdb_rev");
        prev.setIndex(1);
        prev.setIgnore(true);
        radDataForm.addPropertyAnnotation(prev);

        int indice = 2;
        for (Campo campo : campos) {
            PropertyAnnotation p = new PropertyAnnotation();
            p.setCampo(campo);
            p.setIndex(indice);
            p.setEditor(campo.getTipo().toString());
            radDataForm.addPropertyAnnotation(p);
            indice++;
        }
        return radDataForm;

    }


    public DynamicForm formularioToDynamicform(Formulario formulario){

        DynamicForm dynamicForm = new DynamicForm();

        //Buscar los campos del formulario
        Campo campoex = new Campo();
        campoex.setFormulario(formulario);
        //Example<Campo> example = Example.of(campoex);
        //List<Campo> campos = campoRepository.findAll(example);
        List<Campo> campos = campoRepository.findAllByFormulario_IdOrderByPosicion(formulario.getId());

        DynamicComponent pid = new DynamicComponent("formGroup", "formGroup1", "Form section 1");
        pid.setIndex(0);
        pid.setIgnore(false);

        int indice = 2;
        for (Campo campo : campos) {
            DynamicComponent p = new DynamicComponent();
            p.setName("textField");
            p.setIndex(indice);
            p.setLabel(campo.getNombreCampo());
            p.setId(campo.getFieldname());
            pid.addChildren(p);
            indice++;
            //log.debug("campo  {}", campo.toString());
        }
        dynamicForm.addChildren(pid);
        return dynamicForm;

    }

    public ModelNanoSQL formularioToModelNanoSQL(String database, Formulario formulario){

        ModelNanoSQL modelNanoSQL = new ModelNanoSQL(database);
        //Buscar los campos del formulario
        Campo campoex = new Campo();
        campoex.setFormulario(formulario);
        //Example<Campo> example = Example.of(campoex);
        //List<Campo> campos = campoRepository.findAll(example);
        List<Campo> campos = campoRepository.findAllByFormulario_IdOrderByPosicion(formulario.getId());
        Map model = new HashMap();
        Map pid = new HashMap<String,Map>();
        pid.put("pk", true);
        pid.put("ai", true);
        model.put("id:uuid", pid);
        model.put("couchdb_id:string", new HashMap<String,Map>());
        model.put("couchdb_rev:string", new HashMap<String,Map>());
        for (Campo campo : campos) {
            Map p = new HashMap<String,Map>();
            TIPO tipo = campo.getTipo();
            String nombre = campo.getFieldname();
            String tipodata = ConvertUtil.tipoToDatatype(tipo);
            model.put(nombre + ":" + tipodata, p);
        }

        modelNanoSQL.setModel(model);

        return modelNanoSQL;

    }

    public Map formularioToNewData(Formulario formulario){

        Campo campoex = new Campo();
        campoex.setFormulario(formulario);
        Example<Campo> example = Example.of(campoex);
        List<Campo> campos = campoRepository.findAll(example);
        HashMap<String, Object> model = new HashMap();
        model.put("couchdb_id", "");
        model.put("couchdb_rev", "");
        for (Campo campo : campos) {
            String nombre = campo.getFieldname();
            model.put(nombre, "");
        }

        return model;

    }
}
