package com.roviol.repository;

import com.roviol.domain.Perfil;
import com.roviol.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Perfil entity.
 */
@SuppressWarnings("unused")
@Repository
public interface PerfilRepository extends JpaRepository<Perfil, Long> {

    @Query("select perfil from Perfil perfil where perfil.user.login = ?#{principal.username}")
    Page<Perfil> findByUserIsCurrentUser(Pageable page);

    @Query("select perfil from Perfil perfil where perfil.user.login = ?#{principal.username}")
    Optional<Perfil> findByUserIsCurrentUser();

    @Query("select perfil from Perfil perfil where perfil.id = :Id and perfil.user.login = ?#{principal.username}")
    Optional<Perfil> findByUserIsCurrentUserAndId(@Param("Id") Long Id);

    Optional<Perfil> findAllByUser(User user);

}
