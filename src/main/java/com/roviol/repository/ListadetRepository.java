package com.roviol.repository;

import com.roviol.domain.Listadet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the Listadet entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListadetRepository extends JpaRepository<Listadet, Long> {

    Page<Listadet> findAllByListado_Id(Pageable page, Long id);

    @Query("select listadet from Listadet listadet where listadet.listado.user.login = ?#{principal.username}")
    Page<Listadet> findByUserIsCurrentUser(Pageable page);

}
