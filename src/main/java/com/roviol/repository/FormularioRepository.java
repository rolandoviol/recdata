package com.roviol.repository;

import com.roviol.domain.Formulario;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data  repository for the Formulario entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FormularioRepository extends JpaRepository<Formulario, Long> {

    @Query("select formulario from Formulario formulario where formulario.user.login = ?#{principal.username}")
    List<Formulario> findByUserIsCurrentUser();

    @Query("select formulario from Formulario formulario where formulario.id = :Id and formulario.user.login = ?#{principal.username}")
    Optional<Formulario> findByUserIsCurrentUserAndId(@Param("Id") Long Id);
}
