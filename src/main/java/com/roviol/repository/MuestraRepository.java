package com.roviol.repository;

import com.roviol.domain.Formulario;
import com.roviol.domain.Muestra;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import org.springframework.data.domain.Pageable;

import java.util.Optional;
import java.util.Set;

/**
 * Spring Data  repository for the Muestra entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MuestraRepository extends JpaRepository<Muestra, Long> {

    @Query("select muestra from Muestra muestra where muestra.user.login = ?#{principal.username}")
    Page<Muestra> findByUserIsCurrentUser(Pageable page);

    @Query("select muestra from Muestra muestra where muestra.id = :Id and muestra.user.login = ?#{principal.username}")
    Optional<Muestra> findByUserIsCurrentUserAndId(@Param("Id") Long Id);

    Set<Muestra> findAllByFormulario(Formulario formulario);

}
