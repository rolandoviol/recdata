package com.roviol.repository;

import com.roviol.domain.Campo;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;


/**
 * Spring Data  repository for the Campo entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CampoRepository extends JpaRepository<Campo, Long> {

    List<Campo> findAllByFormulario_IdOrderByPosicion(Long id);
    List<Campo> findAllByFormulario_Id(Long id);
    @Query("select campo from Campo campo where campo.id = :Id and campo.formulario.id = :FormId")
    Optional<Campo> findByIdAAndFormulario_Id(@Param("Id") Long Id, @Param("FormId") Long formid);
    //Optional<Campo> findByIdAAndFormulario_Id(Long id, Long formid);

}
