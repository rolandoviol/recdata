package com.roviol.repository;

import com.roviol.domain.Listado;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the Listado entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ListadoRepository extends JpaRepository<Listado, Long> {
/*
    @Query("select listado from Listado listado where listado.user.login = ?#{principal.username}")
    List<Listado> findByUserIsCurrentUser();
*/
    @Query("select listado from Listado listado where listado.user.login = ?#{principal.username}")
    Page<Listado> findByUserIsCurrentUser(Pageable page);

    @Query("select listado from Listado listado where listado.id = :Id and listado.user.login = ?#{principal.username}")
    Optional<Listado> findByUserIsCurrentUserAndId(@Param("Id") Long Id);


}
