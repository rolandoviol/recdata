package com.roviol.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CouchDBConfiguration {

    @Value("${spring.couchdb.uri}")
    private String url;
    @Value("${spring.couchdb.user}")
    private String user;
    @Value("${spring.couchdb.password}")
    private String password;
    @Value("${spring.couchdb.prefijo.db}")
    private String prefijo_db;
    @Value("${spring.couchdb.prefijo.user}")
    private String prefijo_user;
    @Value("${spring.couchdb.prefijo.user}")
    private String prefijo_form;

    public CouchDBConfiguration() {
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPrefijo_db() {
        return prefijo_db;
    }

    public void setPrefijo_db(String prefijo_db) {
        this.prefijo_db = prefijo_db;
    }

    public String getPrefijo_user() {
        return prefijo_user;
    }

    public void setPrefijo_user(String prefijo_user) {
        this.prefijo_user = prefijo_user;
    }

    public String getPrefijo_form() {
        return prefijo_form;
    }

    public void setPrefijo_form(String prefijo_form) {
        this.prefijo_form = prefijo_form;
    }
}
