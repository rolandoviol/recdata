#!/bin/bash

if [ "$#" -ne 1 ]; then
	echo "usar: ./nginx_cert.sh servidor.dominio.com"
	exit 2
fi
    sed "s/servidor.dominio.net dominio.net/$1/" < nginx_ejemplo.conf >  nginx.conf
    sed "s/servidor.dominio.net -d domino.net/$1/" < certificado_ejemplo.sh >  certificado.sh

    mkdir -p letsencript
    mkdir -p log
    mkdir -p certificados

    docker run --rm \
        -v $PWD/nginx.conf:/etc/nginx/conf.d/default.conf \
        -v $PWD/letsencript:/etc/letsencrypt \
        -v $PWD/log:/var/log/letsencrypt \
        -v $PWD/certificado.sh:/root/certificado.sh \
	-p 80:80 \
	-ti nginx:alpine sh /root/certificado.sh

    rm nginx.conf
    rm certificado.sh

    sudo cp letsencript/live/$1/fullchain.pem certificados/
    sudo cp letsencript/live/$1/privkey.pem certificados/

