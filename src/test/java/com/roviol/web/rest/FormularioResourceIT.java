package com.roviol.web.rest;

import com.roviol.RecdataApp;
import com.roviol.domain.Formulario;
import com.roviol.repository.CampoRepository;
import com.roviol.repository.FormularioRepository;
import com.roviol.repository.MuestraRepository;
import com.roviol.repository.UserRepository;
import com.roviol.service.CouchDBService;
import com.roviol.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static com.roviol.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FormularioResource} REST controller.
 */
@SpringBootTest(classes = RecdataApp.class)
public class FormularioResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final String DEFAULT_REVISION = "AAAAAAAAAA";
    private static final String UPDATED_REVISION = "BBBBBBBBBB";

    private static final Instant DEFAULT_CREADO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREADO = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_CREADO = Instant.ofEpochMilli(-1L);

    private static final Instant DEFAULT_MODIFICADO = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_MODIFICADO = Instant.now().truncatedTo(ChronoUnit.MILLIS);
    private static final Instant SMALLER_MODIFICADO = Instant.ofEpochMilli(-1L);

    @Autowired
    private FormularioRepository formularioRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restFormularioMockMvc;

    private Formulario formulario;
    private UserRepository userRepository;
    private CampoRepository campoRepository;
    private CouchDBService couchDBService;
    private MuestraRepository muestraRepository;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FormularioResource formularioResource = new FormularioResource(formularioRepository, userRepository, campoRepository, couchDBService, muestraRepository);
        this.restFormularioMockMvc = MockMvcBuilders.standaloneSetup(formularioResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Formulario createEntity(EntityManager em) {
        Formulario formulario = new Formulario()
            .nombre(DEFAULT_NOMBRE)
            .descripcion(DEFAULT_DESCRIPCION)
            .revision(DEFAULT_REVISION)
            .creado(DEFAULT_CREADO)
            .modificado(DEFAULT_MODIFICADO);
        return formulario;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Formulario createUpdatedEntity(EntityManager em) {
        Formulario formulario = new Formulario()
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .revision(UPDATED_REVISION)
            .creado(UPDATED_CREADO)
            .modificado(UPDATED_MODIFICADO);
        return formulario;
    }

    @BeforeEach
    public void initTest() {
        formulario = createEntity(em);
    }

    @Test
    @Transactional
    public void createFormulario() throws Exception {
        int databaseSizeBeforeCreate = formularioRepository.findAll().size();

        // Create the Formulario
        restFormularioMockMvc.perform(post("/api/formularios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(formulario)))
            .andExpect(status().isCreated());

        // Validate the Formulario in the database
        List<Formulario> formularioList = formularioRepository.findAll();
        assertThat(formularioList).hasSize(databaseSizeBeforeCreate + 1);
        Formulario testFormulario = formularioList.get(formularioList.size() - 1);
        assertThat(testFormulario.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testFormulario.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testFormulario.getRevision()).isEqualTo(DEFAULT_REVISION);
        assertThat(testFormulario.getCreado()).isEqualTo(DEFAULT_CREADO);
        assertThat(testFormulario.getModificado()).isEqualTo(DEFAULT_MODIFICADO);
    }

    @Test
    @Transactional
    public void createFormularioWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = formularioRepository.findAll().size();

        // Create the Formulario with an existing ID
        formulario.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFormularioMockMvc.perform(post("/api/formularios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(formulario)))
            .andExpect(status().isBadRequest());

        // Validate the Formulario in the database
        List<Formulario> formularioList = formularioRepository.findAll();
        assertThat(formularioList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = formularioRepository.findAll().size();
        // set the field null
        formulario.setNombre(null);

        // Create the Formulario, which fails.

        restFormularioMockMvc.perform(post("/api/formularios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(formulario)))
            .andExpect(status().isBadRequest());

        List<Formulario> formularioList = formularioRepository.findAll();
        assertThat(formularioList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllFormularios() throws Exception {
        // Initialize the database
        formularioRepository.saveAndFlush(formulario);

        // Get all the formularioList
        restFormularioMockMvc.perform(get("/api/formularios?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(formulario.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())))
            .andExpect(jsonPath("$.[*].revision").value(hasItem(DEFAULT_REVISION.toString())))
            .andExpect(jsonPath("$.[*].creado").value(hasItem(DEFAULT_CREADO.toString())))
            .andExpect(jsonPath("$.[*].modificado").value(hasItem(DEFAULT_MODIFICADO.toString())));
    }

    @Test
    @Transactional
    public void getFormulario() throws Exception {
        // Initialize the database
        formularioRepository.saveAndFlush(formulario);

        // Get the formulario
        restFormularioMockMvc.perform(get("/api/formularios/{id}", formulario.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(formulario.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()))
            .andExpect(jsonPath("$.revision").value(DEFAULT_REVISION.toString()))
            .andExpect(jsonPath("$.creado").value(DEFAULT_CREADO.toString()))
            .andExpect(jsonPath("$.modificado").value(DEFAULT_MODIFICADO.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingFormulario() throws Exception {
        // Get the formulario
        restFormularioMockMvc.perform(get("/api/formularios/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFormulario() throws Exception {
        // Initialize the database
        formularioRepository.saveAndFlush(formulario);

        int databaseSizeBeforeUpdate = formularioRepository.findAll().size();

        // Update the formulario
        Formulario updatedFormulario = formularioRepository.findById(formulario.getId()).get();
        // Disconnect from session so that the updates on updatedFormulario are not directly saved in db
        em.detach(updatedFormulario);
        updatedFormulario
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .revision(UPDATED_REVISION)
            .creado(UPDATED_CREADO)
            .modificado(UPDATED_MODIFICADO);

        restFormularioMockMvc.perform(put("/api/formularios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFormulario)))
            .andExpect(status().isOk());

        // Validate the Formulario in the database
        List<Formulario> formularioList = formularioRepository.findAll();
        assertThat(formularioList).hasSize(databaseSizeBeforeUpdate);
        Formulario testFormulario = formularioList.get(formularioList.size() - 1);
        assertThat(testFormulario.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testFormulario.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testFormulario.getRevision()).isEqualTo(UPDATED_REVISION);
        assertThat(testFormulario.getCreado()).isEqualTo(UPDATED_CREADO);
        assertThat(testFormulario.getModificado()).isEqualTo(UPDATED_MODIFICADO);
    }

    @Test
    @Transactional
    public void updateNonExistingFormulario() throws Exception {
        int databaseSizeBeforeUpdate = formularioRepository.findAll().size();

        // Create the Formulario

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFormularioMockMvc.perform(put("/api/formularios")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(formulario)))
            .andExpect(status().isBadRequest());

        // Validate the Formulario in the database
        List<Formulario> formularioList = formularioRepository.findAll();
        assertThat(formularioList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFormulario() throws Exception {
        // Initialize the database
        formularioRepository.saveAndFlush(formulario);

        int databaseSizeBeforeDelete = formularioRepository.findAll().size();

        // Delete the formulario
        restFormularioMockMvc.perform(delete("/api/formularios/{id}", formulario.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Formulario> formularioList = formularioRepository.findAll();
        assertThat(formularioList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Formulario.class);
        Formulario formulario1 = new Formulario();
        formulario1.setId(1L);
        Formulario formulario2 = new Formulario();
        formulario2.setId(formulario1.getId());
        assertThat(formulario1).isEqualTo(formulario2);
        formulario2.setId(2L);
        assertThat(formulario1).isNotEqualTo(formulario2);
        formulario1.setId(null);
        assertThat(formulario1).isNotEqualTo(formulario2);
    }
}
