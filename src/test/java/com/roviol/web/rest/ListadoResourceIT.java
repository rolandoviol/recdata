package com.roviol.web.rest;

import com.roviol.RecdataApp;
import com.roviol.domain.Listado;
import com.roviol.repository.ListadoRepository;
import com.roviol.repository.UserRepository;
import com.roviol.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.roviol.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ListadoResource} REST controller.
 */
@SpringBootTest(classes = RecdataApp.class)
public class ListadoResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ListadoRepository listadoRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restListadoMockMvc;

    private Listado listado;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ListadoResource listadoResource = new ListadoResource(listadoRepository, userRepository);
        this.restListadoMockMvc = MockMvcBuilders.standaloneSetup(listadoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Listado createEntity(EntityManager em) {
        Listado listado = new Listado()
            .name(DEFAULT_NAME);
        return listado;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Listado createUpdatedEntity(EntityManager em) {
        Listado listado = new Listado()
            .name(UPDATED_NAME);
        return listado;
    }

    @BeforeEach
    public void initTest() {
        listado = createEntity(em);
    }

    @Test
    @Transactional
    public void createListado() throws Exception {
        int databaseSizeBeforeCreate = listadoRepository.findAll().size();

        // Create the Listado
        restListadoMockMvc.perform(post("/api/listados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listado)))
            .andExpect(status().isCreated());

        // Validate the Listado in the database
        List<Listado> listadoList = listadoRepository.findAll();
        assertThat(listadoList).hasSize(databaseSizeBeforeCreate + 1);
        Listado testListado = listadoList.get(listadoList.size() - 1);
        assertThat(testListado.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createListadoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = listadoRepository.findAll().size();

        // Create the Listado with an existing ID
        listado.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restListadoMockMvc.perform(post("/api/listados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listado)))
            .andExpect(status().isBadRequest());

        // Validate the Listado in the database
        List<Listado> listadoList = listadoRepository.findAll();
        assertThat(listadoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = listadoRepository.findAll().size();
        // set the field null
        listado.setName(null);

        // Create the Listado, which fails.

        restListadoMockMvc.perform(post("/api/listados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listado)))
            .andExpect(status().isBadRequest());

        List<Listado> listadoList = listadoRepository.findAll();
        assertThat(listadoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllListados() throws Exception {
        // Initialize the database
        listadoRepository.saveAndFlush(listado);

        // Get all the listadoList
        restListadoMockMvc.perform(get("/api/listados?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listado.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getListado() throws Exception {
        // Initialize the database
        listadoRepository.saveAndFlush(listado);

        // Get the listado
        restListadoMockMvc.perform(get("/api/listados/{id}", listado.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(listado.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingListado() throws Exception {
        // Get the listado
        restListadoMockMvc.perform(get("/api/listados/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateListado() throws Exception {
        // Initialize the database
        listadoRepository.saveAndFlush(listado);

        int databaseSizeBeforeUpdate = listadoRepository.findAll().size();

        // Update the listado
        Listado updatedListado = listadoRepository.findById(listado.getId()).get();
        // Disconnect from session so that the updates on updatedListado are not directly saved in db
        em.detach(updatedListado);
        updatedListado
            .name(UPDATED_NAME);

        restListadoMockMvc.perform(put("/api/listados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedListado)))
            .andExpect(status().isOk());

        // Validate the Listado in the database
        List<Listado> listadoList = listadoRepository.findAll();
        assertThat(listadoList).hasSize(databaseSizeBeforeUpdate);
        Listado testListado = listadoList.get(listadoList.size() - 1);
        assertThat(testListado.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingListado() throws Exception {
        int databaseSizeBeforeUpdate = listadoRepository.findAll().size();

        // Create the Listado

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListadoMockMvc.perform(put("/api/listados")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listado)))
            .andExpect(status().isBadRequest());

        // Validate the Listado in the database
        List<Listado> listadoList = listadoRepository.findAll();
        assertThat(listadoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteListado() throws Exception {
        // Initialize the database
        listadoRepository.saveAndFlush(listado);

        int databaseSizeBeforeDelete = listadoRepository.findAll().size();

        // Delete the listado
        restListadoMockMvc.perform(delete("/api/listados/{id}", listado.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Listado> listadoList = listadoRepository.findAll();
        assertThat(listadoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Listado.class);
        Listado listado1 = new Listado();
        listado1.setId(1L);
        Listado listado2 = new Listado();
        listado2.setId(listado1.getId());
        assertThat(listado1).isEqualTo(listado2);
        listado2.setId(2L);
        assertThat(listado1).isNotEqualTo(listado2);
        listado1.setId(null);
        assertThat(listado1).isNotEqualTo(listado2);
    }
}
