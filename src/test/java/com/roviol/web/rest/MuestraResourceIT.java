package com.roviol.web.rest;

import com.roviol.RecdataApp;
import com.roviol.domain.Muestra;
import com.roviol.repository.MuestraRepository;
import com.roviol.repository.UserRepository;
import com.roviol.service.CouchDBService;
import com.roviol.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static com.roviol.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MuestraResource} REST controller.
 */
@SpringBootTest(classes = RecdataApp.class)
public class MuestraResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final byte[] DEFAULT_LOGO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_LOGO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_LOGO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_LOGO_CONTENT_TYPE = "image/png";

    private static final LocalDate DEFAULT_INICIO = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_INICIO = LocalDate.now(ZoneId.systemDefault());
    private static final LocalDate SMALLER_INICIO = LocalDate.ofEpochDay(-1L);

    private static final Boolean DEFAULT_ACTIVO = false;
    private static final Boolean UPDATED_ACTIVO = true;

    private static final String DEFAULT_DATABASE_NAME = "AAAAAAAAAA";
    private static final String UPDATED_DATABASE_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DATABASE_KEY = "AAAAAAAAAA";
    private static final String UPDATED_DATABASE_KEY = "BBBBBBBBBB";

    private static final String DEFAULT_DATABASE_SECRET = "AAAAAAAAAA";
    private static final String UPDATED_DATABASE_SECRET = "BBBBBBBBBB";

    @Autowired
    private MuestraRepository muestraRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restMuestraMockMvc;

    private Muestra muestra;
    private UserRepository userRepository;
    private CouchDBService couchDBService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final MuestraResource muestraResource = new MuestraResource(muestraRepository, userRepository, couchDBService);
        this.restMuestraMockMvc = MockMvcBuilders.standaloneSetup(muestraResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Muestra createEntity(EntityManager em) {
        Muestra muestra = new Muestra()
            .nombre(DEFAULT_NOMBRE)
            .logo(DEFAULT_LOGO)
            .logoContentType(DEFAULT_LOGO_CONTENT_TYPE)
            .inicio(DEFAULT_INICIO)
            .activo(DEFAULT_ACTIVO)
            .databaseName(DEFAULT_DATABASE_NAME)
            .databaseKey(DEFAULT_DATABASE_KEY)
            .databaseSecret(DEFAULT_DATABASE_SECRET);
        return muestra;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Muestra createUpdatedEntity(EntityManager em) {
        Muestra muestra = new Muestra()
            .nombre(UPDATED_NOMBRE)
            .logo(UPDATED_LOGO)
            .logoContentType(UPDATED_LOGO_CONTENT_TYPE)
            .inicio(UPDATED_INICIO)
            .activo(UPDATED_ACTIVO)
            .databaseName(UPDATED_DATABASE_NAME)
            .databaseKey(UPDATED_DATABASE_KEY)
            .databaseSecret(UPDATED_DATABASE_SECRET);
        return muestra;
    }

    @BeforeEach
    public void initTest() {
        muestra = createEntity(em);
    }

    @Test
    @Transactional
    public void createMuestra() throws Exception {
        int databaseSizeBeforeCreate = muestraRepository.findAll().size();

        // Create the Muestra
        restMuestraMockMvc.perform(post("/api/muestras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(muestra)))
            .andExpect(status().isCreated());

        // Validate the Muestra in the database
        List<Muestra> muestraList = muestraRepository.findAll();
        assertThat(muestraList).hasSize(databaseSizeBeforeCreate + 1);
        Muestra testMuestra = muestraList.get(muestraList.size() - 1);
        assertThat(testMuestra.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testMuestra.getLogo()).isEqualTo(DEFAULT_LOGO);
        assertThat(testMuestra.getLogoContentType()).isEqualTo(DEFAULT_LOGO_CONTENT_TYPE);
        assertThat(testMuestra.getInicio()).isEqualTo(DEFAULT_INICIO);
        assertThat(testMuestra.isActivo()).isEqualTo(DEFAULT_ACTIVO);
        assertThat(testMuestra.getDatabaseName()).isEqualTo(DEFAULT_DATABASE_NAME);
        assertThat(testMuestra.getDatabaseKey()).isEqualTo(DEFAULT_DATABASE_KEY);
        assertThat(testMuestra.getDatabaseSecret()).isEqualTo(DEFAULT_DATABASE_SECRET);
    }

    @Test
    @Transactional
    public void createMuestraWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = muestraRepository.findAll().size();

        // Create the Muestra with an existing ID
        muestra.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMuestraMockMvc.perform(post("/api/muestras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(muestra)))
            .andExpect(status().isBadRequest());

        // Validate the Muestra in the database
        List<Muestra> muestraList = muestraRepository.findAll();
        assertThat(muestraList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = muestraRepository.findAll().size();
        // set the field null
        muestra.setNombre(null);

        // Create the Muestra, which fails.

        restMuestraMockMvc.perform(post("/api/muestras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(muestra)))
            .andExpect(status().isBadRequest());

        List<Muestra> muestraList = muestraRepository.findAll();
        assertThat(muestraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkActivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = muestraRepository.findAll().size();
        // set the field null
        muestra.setActivo(null);

        // Create the Muestra, which fails.

        restMuestraMockMvc.perform(post("/api/muestras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(muestra)))
            .andExpect(status().isBadRequest());

        List<Muestra> muestraList = muestraRepository.findAll();
        assertThat(muestraList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllMuestras() throws Exception {
        // Initialize the database
        muestraRepository.saveAndFlush(muestra);

        // Get all the muestraList
        restMuestraMockMvc.perform(get("/api/muestras?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(muestra.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE.toString())))
            .andExpect(jsonPath("$.[*].logoContentType").value(hasItem(DEFAULT_LOGO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].logo").value(hasItem(Base64Utils.encodeToString(DEFAULT_LOGO))))
            .andExpect(jsonPath("$.[*].inicio").value(hasItem(DEFAULT_INICIO.toString())))
            .andExpect(jsonPath("$.[*].activo").value(hasItem(DEFAULT_ACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].databaseName").value(hasItem(DEFAULT_DATABASE_NAME.toString())))
            .andExpect(jsonPath("$.[*].databaseKey").value(hasItem(DEFAULT_DATABASE_KEY.toString())))
            .andExpect(jsonPath("$.[*].databaseSecret").value(hasItem(DEFAULT_DATABASE_SECRET.toString())));
    }

    @Test
    @Transactional
    public void getMuestra() throws Exception {
        // Initialize the database
        muestraRepository.saveAndFlush(muestra);

        // Get the muestra
        restMuestraMockMvc.perform(get("/api/muestras/{id}", muestra.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(muestra.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE.toString()))
            .andExpect(jsonPath("$.logoContentType").value(DEFAULT_LOGO_CONTENT_TYPE))
            .andExpect(jsonPath("$.logo").value(Base64Utils.encodeToString(DEFAULT_LOGO)))
            .andExpect(jsonPath("$.inicio").value(DEFAULT_INICIO.toString()))
            .andExpect(jsonPath("$.activo").value(DEFAULT_ACTIVO.booleanValue()))
            .andExpect(jsonPath("$.databaseName").value(DEFAULT_DATABASE_NAME.toString()))
            .andExpect(jsonPath("$.databaseKey").value(DEFAULT_DATABASE_KEY.toString()))
            .andExpect(jsonPath("$.databaseSecret").value(DEFAULT_DATABASE_SECRET.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingMuestra() throws Exception {
        // Get the muestra
        restMuestraMockMvc.perform(get("/api/muestras/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMuestra() throws Exception {
        // Initialize the database
        muestraRepository.saveAndFlush(muestra);

        int databaseSizeBeforeUpdate = muestraRepository.findAll().size();

        // Update the muestra
        Muestra updatedMuestra = muestraRepository.findById(muestra.getId()).get();
        // Disconnect from session so that the updates on updatedMuestra are not directly saved in db
        em.detach(updatedMuestra);
        updatedMuestra
            .nombre(UPDATED_NOMBRE)
            .logo(UPDATED_LOGO)
            .logoContentType(UPDATED_LOGO_CONTENT_TYPE)
            .inicio(UPDATED_INICIO)
            .activo(UPDATED_ACTIVO)
            .databaseName(UPDATED_DATABASE_NAME)
            .databaseKey(UPDATED_DATABASE_KEY)
            .databaseSecret(UPDATED_DATABASE_SECRET);

        restMuestraMockMvc.perform(put("/api/muestras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedMuestra)))
            .andExpect(status().isOk());

        // Validate the Muestra in the database
        List<Muestra> muestraList = muestraRepository.findAll();
        assertThat(muestraList).hasSize(databaseSizeBeforeUpdate);
        Muestra testMuestra = muestraList.get(muestraList.size() - 1);
        assertThat(testMuestra.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testMuestra.getLogo()).isEqualTo(UPDATED_LOGO);
        assertThat(testMuestra.getLogoContentType()).isEqualTo(UPDATED_LOGO_CONTENT_TYPE);
        assertThat(testMuestra.getInicio()).isEqualTo(UPDATED_INICIO);
        assertThat(testMuestra.isActivo()).isEqualTo(UPDATED_ACTIVO);
        assertThat(testMuestra.getDatabaseName()).isEqualTo(UPDATED_DATABASE_NAME);
        assertThat(testMuestra.getDatabaseKey()).isEqualTo(UPDATED_DATABASE_KEY);
        assertThat(testMuestra.getDatabaseSecret()).isEqualTo(UPDATED_DATABASE_SECRET);
    }

    @Test
    @Transactional
    public void updateNonExistingMuestra() throws Exception {
        int databaseSizeBeforeUpdate = muestraRepository.findAll().size();

        // Create the Muestra

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMuestraMockMvc.perform(put("/api/muestras")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(muestra)))
            .andExpect(status().isBadRequest());

        // Validate the Muestra in the database
        List<Muestra> muestraList = muestraRepository.findAll();
        assertThat(muestraList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMuestra() throws Exception {
        // Initialize the database
        muestraRepository.saveAndFlush(muestra);

        int databaseSizeBeforeDelete = muestraRepository.findAll().size();

        // Delete the muestra
        restMuestraMockMvc.perform(delete("/api/muestras/{id}", muestra.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Muestra> muestraList = muestraRepository.findAll();
        assertThat(muestraList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Muestra.class);
        Muestra muestra1 = new Muestra();
        muestra1.setId(1L);
        Muestra muestra2 = new Muestra();
        muestra2.setId(muestra1.getId());
        assertThat(muestra1).isEqualTo(muestra2);
        muestra2.setId(2L);
        assertThat(muestra1).isNotEqualTo(muestra2);
        muestra1.setId(null);
        assertThat(muestra1).isNotEqualTo(muestra2);
    }
}
