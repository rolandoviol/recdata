package com.roviol.web.rest;

import com.roviol.RecdataApp;
import com.roviol.domain.Listadet;
import com.roviol.domain.Listado;
import com.roviol.repository.ListadetRepository;
import com.roviol.repository.ListadoRepository;
import com.roviol.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.roviol.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ListadetResource} REST controller.
 */
@SpringBootTest(classes = RecdataApp.class)
public class ListadetResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private ListadetRepository listadetRepository;

    @Autowired
    private ListadoRepository listadoRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restListadetMockMvc;

    private Listadet listadet;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ListadetResource listadetResource = new ListadetResource(listadetRepository, listadoRepository);
        this.restListadetMockMvc = MockMvcBuilders.standaloneSetup(listadetResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Listadet createEntity(EntityManager em) {
        Listadet listadet = new Listadet()
            .name(DEFAULT_NAME);
        // Add required entity
        Listado listado;
        if (TestUtil.findAll(em, Listado.class).isEmpty()) {
            listado = ListadoResourceIT.createEntity(em);
            em.persist(listado);
            em.flush();
        } else {
            listado = TestUtil.findAll(em, Listado.class).get(0);
        }
        listadet.setListado(listado);
        return listadet;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Listadet createUpdatedEntity(EntityManager em) {
        Listadet listadet = new Listadet()
            .name(UPDATED_NAME);
        // Add required entity
        Listado listado;
        if (TestUtil.findAll(em, Listado.class).isEmpty()) {
            listado = ListadoResourceIT.createUpdatedEntity(em);
            em.persist(listado);
            em.flush();
        } else {
            listado = TestUtil.findAll(em, Listado.class).get(0);
        }
        listadet.setListado(listado);
        return listadet;
    }

    @BeforeEach
    public void initTest() {
        listadet = createEntity(em);
    }

    @Test
    @Transactional
    public void createListadet() throws Exception {
        int databaseSizeBeforeCreate = listadetRepository.findAll().size();

        // Create the Listadet
        restListadetMockMvc.perform(post("/api/listadets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listadet)))
            .andExpect(status().isCreated());

        // Validate the Listadet in the database
        List<Listadet> listadetList = listadetRepository.findAll();
        assertThat(listadetList).hasSize(databaseSizeBeforeCreate + 1);
        Listadet testListadet = listadetList.get(listadetList.size() - 1);
        assertThat(testListadet.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createListadetWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = listadetRepository.findAll().size();

        // Create the Listadet with an existing ID
        listadet.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restListadetMockMvc.perform(post("/api/listadets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listadet)))
            .andExpect(status().isBadRequest());

        // Validate the Listadet in the database
        List<Listadet> listadetList = listadetRepository.findAll();
        assertThat(listadetList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = listadetRepository.findAll().size();
        // set the field null
        listadet.setName(null);

        // Create the Listadet, which fails.

        restListadetMockMvc.perform(post("/api/listadets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listadet)))
            .andExpect(status().isBadRequest());

        List<Listadet> listadetList = listadetRepository.findAll();
        assertThat(listadetList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllListadets() throws Exception {
        // Initialize the database
        listadetRepository.saveAndFlush(listadet);

        // Get all the listadetList
        restListadetMockMvc.perform(get("/api/listadets?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(listadet.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getListadet() throws Exception {
        // Initialize the database
        listadetRepository.saveAndFlush(listadet);

        // Get the listadet
        restListadetMockMvc.perform(get("/api/listadets/{id}", listadet.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(listadet.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingListadet() throws Exception {
        // Get the listadet
        restListadetMockMvc.perform(get("/api/listadets/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateListadet() throws Exception {
        // Initialize the database
        listadetRepository.saveAndFlush(listadet);

        int databaseSizeBeforeUpdate = listadetRepository.findAll().size();

        // Update the listadet
        Listadet updatedListadet = listadetRepository.findById(listadet.getId()).get();
        // Disconnect from session so that the updates on updatedListadet are not directly saved in db
        em.detach(updatedListadet);
        updatedListadet
            .name(UPDATED_NAME);

        restListadetMockMvc.perform(put("/api/listadets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedListadet)))
            .andExpect(status().isOk());

        // Validate the Listadet in the database
        List<Listadet> listadetList = listadetRepository.findAll();
        assertThat(listadetList).hasSize(databaseSizeBeforeUpdate);
        Listadet testListadet = listadetList.get(listadetList.size() - 1);
        assertThat(testListadet.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingListadet() throws Exception {
        int databaseSizeBeforeUpdate = listadetRepository.findAll().size();

        // Create the Listadet

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restListadetMockMvc.perform(put("/api/listadets")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(listadet)))
            .andExpect(status().isBadRequest());

        // Validate the Listadet in the database
        List<Listadet> listadetList = listadetRepository.findAll();
        assertThat(listadetList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteListadet() throws Exception {
        // Initialize the database
        listadetRepository.saveAndFlush(listadet);

        int databaseSizeBeforeDelete = listadetRepository.findAll().size();

        // Delete the listadet
        restListadetMockMvc.perform(delete("/api/listadets/{id}", listadet.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Listadet> listadetList = listadetRepository.findAll();
        assertThat(listadetList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Listadet.class);
        Listadet listadet1 = new Listadet();
        listadet1.setId(1L);
        Listadet listadet2 = new Listadet();
        listadet2.setId(listadet1.getId());
        assertThat(listadet1).isEqualTo(listadet2);
        listadet2.setId(2L);
        assertThat(listadet1).isNotEqualTo(listadet2);
        listadet1.setId(null);
        assertThat(listadet1).isNotEqualTo(listadet2);
    }
}
