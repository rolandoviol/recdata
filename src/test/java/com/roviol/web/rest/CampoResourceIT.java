package com.roviol.web.rest;

import com.roviol.RecdataApp;
import com.roviol.domain.Campo;
import com.roviol.repository.CampoRepository;
import com.roviol.repository.FormularioRepository;
import com.roviol.web.rest.errors.ExceptionTranslator;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Validator;

import javax.persistence.EntityManager;
import java.util.List;

import static com.roviol.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.roviol.domain.enumeration.TIPO;
/**
 * Integration tests for the {@link CampoResource} REST controller.
 */
@SpringBootTest(classes = RecdataApp.class)
public class CampoResourceIT {

    private static final String DEFAULT_NOMBRE_CAMPO = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE_CAMPO = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final TIPO DEFAULT_TIPO = TIPO.Text;
    private static final TIPO UPDATED_TIPO = TIPO.MultilineText;

    private static final Boolean DEFAULT_REQUERIDO = false;
    private static final Boolean UPDATED_REQUERIDO = true;

    private static final Integer DEFAULT_POSICION = 1;
    private static final Integer UPDATED_POSICION = 2;
    private static final Integer SMALLER_POSICION = 1 - 1;

    @Autowired
    private CampoRepository campoRepository;

    @Autowired
    private FormularioRepository formularioRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    @Autowired
    private Validator validator;

    private MockMvc restCampoMockMvc;

    private Campo campo;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final CampoResource campoResource = new CampoResource(campoRepository, formularioRepository);
        this.restCampoMockMvc = MockMvcBuilders.standaloneSetup(campoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter)
            .setValidator(validator).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Campo createEntity(EntityManager em) {
        Campo campo = new Campo()
            .nombreCampo(DEFAULT_NOMBRE_CAMPO)
            .descripcion(DEFAULT_DESCRIPCION)
            .tipo(DEFAULT_TIPO)
            .requerido(DEFAULT_REQUERIDO)
            .posicion(DEFAULT_POSICION);
        return campo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Campo createUpdatedEntity(EntityManager em) {
        Campo campo = new Campo()
            .nombreCampo(UPDATED_NOMBRE_CAMPO)
            .descripcion(UPDATED_DESCRIPCION)
            .tipo(UPDATED_TIPO)
            .requerido(UPDATED_REQUERIDO)
            .posicion(UPDATED_POSICION);
        return campo;
    }

    @BeforeEach
    public void initTest() {
        campo = createEntity(em);
    }

    @Test
    @Transactional
    public void createCampo() throws Exception {
        int databaseSizeBeforeCreate = campoRepository.findAll().size();

        // Create the Campo
        restCampoMockMvc.perform(post("/api/campos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campo)))
            .andExpect(status().isCreated());

        // Validate the Campo in the database
        List<Campo> campoList = campoRepository.findAll();
        assertThat(campoList).hasSize(databaseSizeBeforeCreate + 1);
        Campo testCampo = campoList.get(campoList.size() - 1);
        assertThat(testCampo.getNombreCampo()).isEqualTo(DEFAULT_NOMBRE_CAMPO);
        assertThat(testCampo.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testCampo.getTipo()).isEqualTo(DEFAULT_TIPO);
        assertThat(testCampo.isRequerido()).isEqualTo(DEFAULT_REQUERIDO);
        assertThat(testCampo.getPosicion()).isEqualTo(DEFAULT_POSICION);
    }

    @Test
    @Transactional
    public void createCampoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = campoRepository.findAll().size();

        // Create the Campo with an existing ID
        campo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCampoMockMvc.perform(post("/api/campos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campo)))
            .andExpect(status().isBadRequest());

        // Validate the Campo in the database
        List<Campo> campoList = campoRepository.findAll();
        assertThat(campoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkNombreCampoIsRequired() throws Exception {
        int databaseSizeBeforeTest = campoRepository.findAll().size();
        // set the field null
        campo.setNombreCampo(null);

        // Create the Campo, which fails.

        restCampoMockMvc.perform(post("/api/campos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campo)))
            .andExpect(status().isBadRequest());

        List<Campo> campoList = campoRepository.findAll();
        assertThat(campoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkTipoIsRequired() throws Exception {
        int databaseSizeBeforeTest = campoRepository.findAll().size();
        // set the field null
        campo.setTipo(null);

        // Create the Campo, which fails.

        restCampoMockMvc.perform(post("/api/campos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campo)))
            .andExpect(status().isBadRequest());

        List<Campo> campoList = campoRepository.findAll();
        assertThat(campoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkRequeridoIsRequired() throws Exception {
        int databaseSizeBeforeTest = campoRepository.findAll().size();
        // set the field null
        campo.setRequerido(null);

        // Create the Campo, which fails.

        restCampoMockMvc.perform(post("/api/campos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campo)))
            .andExpect(status().isBadRequest());

        List<Campo> campoList = campoRepository.findAll();
        assertThat(campoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllCampos() throws Exception {
        // Initialize the database
        campoRepository.saveAndFlush(campo);

        // Get all the campoList
        restCampoMockMvc.perform(get("/api/campos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(campo.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombreCampo").value(hasItem(DEFAULT_NOMBRE_CAMPO.toString())))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION.toString())))
            .andExpect(jsonPath("$.[*].tipo").value(hasItem(DEFAULT_TIPO.toString())))
            .andExpect(jsonPath("$.[*].requerido").value(hasItem(DEFAULT_REQUERIDO.booleanValue())))
            .andExpect(jsonPath("$.[*].posicion").value(hasItem(DEFAULT_POSICION)));
    }

    @Test
    @Transactional
    public void getCampo() throws Exception {
        // Initialize the database
        campoRepository.saveAndFlush(campo);

        // Get the campo
        restCampoMockMvc.perform(get("/api/campos/{id}", campo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(campo.getId().intValue()))
            .andExpect(jsonPath("$.nombreCampo").value(DEFAULT_NOMBRE_CAMPO.toString()))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION.toString()))
            .andExpect(jsonPath("$.tipo").value(DEFAULT_TIPO.toString()))
            .andExpect(jsonPath("$.requerido").value(DEFAULT_REQUERIDO.booleanValue()))
            .andExpect(jsonPath("$.posicion").value(DEFAULT_POSICION));
    }

    @Test
    @Transactional
    public void getNonExistingCampo() throws Exception {
        // Get the campo
        restCampoMockMvc.perform(get("/api/campos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCampo() throws Exception {
        // Initialize the database
        campoRepository.saveAndFlush(campo);

        int databaseSizeBeforeUpdate = campoRepository.findAll().size();

        // Update the campo
        Campo updatedCampo = campoRepository.findById(campo.getId()).get();
        // Disconnect from session so that the updates on updatedCampo are not directly saved in db
        em.detach(updatedCampo);
        updatedCampo
            .nombreCampo(UPDATED_NOMBRE_CAMPO)
            .descripcion(UPDATED_DESCRIPCION)
            .tipo(UPDATED_TIPO)
            .requerido(UPDATED_REQUERIDO)
            .posicion(UPDATED_POSICION);

        restCampoMockMvc.perform(put("/api/campos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCampo)))
            .andExpect(status().isOk());

        // Validate the Campo in the database
        List<Campo> campoList = campoRepository.findAll();
        assertThat(campoList).hasSize(databaseSizeBeforeUpdate);
        Campo testCampo = campoList.get(campoList.size() - 1);
        assertThat(testCampo.getNombreCampo()).isEqualTo(UPDATED_NOMBRE_CAMPO);
        assertThat(testCampo.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testCampo.getTipo()).isEqualTo(UPDATED_TIPO);
        assertThat(testCampo.isRequerido()).isEqualTo(UPDATED_REQUERIDO);
        assertThat(testCampo.getPosicion()).isEqualTo(UPDATED_POSICION);
    }

    @Test
    @Transactional
    public void updateNonExistingCampo() throws Exception {
        int databaseSizeBeforeUpdate = campoRepository.findAll().size();

        // Create the Campo

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restCampoMockMvc.perform(put("/api/campos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(campo)))
            .andExpect(status().isBadRequest());

        // Validate the Campo in the database
        List<Campo> campoList = campoRepository.findAll();
        assertThat(campoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteCampo() throws Exception {
        // Initialize the database
        campoRepository.saveAndFlush(campo);

        int databaseSizeBeforeDelete = campoRepository.findAll().size();

        // Delete the campo
        restCampoMockMvc.perform(delete("/api/campos/{id}", campo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Campo> campoList = campoRepository.findAll();
        assertThat(campoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Campo.class);
        Campo campo1 = new Campo();
        campo1.setId(1L);
        Campo campo2 = new Campo();
        campo2.setId(campo1.getId());
        assertThat(campo1).isEqualTo(campo2);
        campo2.setId(2L);
        assertThat(campo1).isNotEqualTo(campo2);
        campo1.setId(null);
        assertThat(campo1).isNotEqualTo(campo2);
    }
}
