/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RecdataTestModule } from '../../../test.module';
import { CampoDeleteDialogComponent } from 'app/entities/campo/campo-delete-dialog.component';
import { CampoService } from 'app/entities/campo/campo.service';

describe('Component Tests', () => {
  describe('Campo Management Delete Component', () => {
    let comp: CampoDeleteDialogComponent;
    let fixture: ComponentFixture<CampoDeleteDialogComponent>;
    let service: CampoService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [CampoDeleteDialogComponent]
      })
        .overrideTemplate(CampoDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CampoDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CampoService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
