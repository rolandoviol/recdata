/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecdataTestModule } from '../../../test.module';
import { CampoDetailComponent } from 'app/entities/campo/campo-detail.component';
import { Campo } from 'app/shared/model/campo.model';

describe('Component Tests', () => {
  describe('Campo Management Detail Component', () => {
    let comp: CampoDetailComponent;
    let fixture: ComponentFixture<CampoDetailComponent>;
    const route = ({ data: of({ campo: new Campo(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [CampoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(CampoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(CampoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.campo).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
