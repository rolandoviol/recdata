/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { RecdataTestModule } from '../../../test.module';
import { CampoComponent } from 'app/entities/campo/campo.component';
import { CampoService } from 'app/entities/campo/campo.service';
import { Campo } from 'app/shared/model/campo.model';

describe('Component Tests', () => {
  describe('Campo Management Component', () => {
    let comp: CampoComponent;
    let fixture: ComponentFixture<CampoComponent>;
    let service: CampoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [CampoComponent],
        providers: []
      })
        .overrideTemplate(CampoComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(CampoComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(CampoService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Campo(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.campos[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
