/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecdataTestModule } from '../../../test.module';
import { ListadetDetailComponent } from 'app/entities/listadet/listadet-detail.component';
import { Listadet } from 'app/shared/model/listadet.model';

describe('Component Tests', () => {
  describe('Listadet Management Detail Component', () => {
    let comp: ListadetDetailComponent;
    let fixture: ComponentFixture<ListadetDetailComponent>;
    const route = ({ data: of({ listadet: new Listadet(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [ListadetDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ListadetDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ListadetDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.listadet).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
