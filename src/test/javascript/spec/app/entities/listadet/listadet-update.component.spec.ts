/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RecdataTestModule } from '../../../test.module';
import { ListadetUpdateComponent } from 'app/entities/listadet/listadet-update.component';
import { ListadetService } from 'app/entities/listadet/listadet.service';
import { Listadet } from 'app/shared/model/listadet.model';

describe('Component Tests', () => {
  describe('Listadet Management Update Component', () => {
    let comp: ListadetUpdateComponent;
    let fixture: ComponentFixture<ListadetUpdateComponent>;
    let service: ListadetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [ListadetUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ListadetUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ListadetUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ListadetService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Listadet(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Listadet();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
