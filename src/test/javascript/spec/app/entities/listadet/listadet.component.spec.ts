/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { RecdataTestModule } from '../../../test.module';
import { ListadetComponent } from 'app/entities/listadet/listadet.component';
import { ListadetService } from 'app/entities/listadet/listadet.service';
import { Listadet } from 'app/shared/model/listadet.model';

describe('Component Tests', () => {
  describe('Listadet Management Component', () => {
    let comp: ListadetComponent;
    let fixture: ComponentFixture<ListadetComponent>;
    let service: ListadetService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [ListadetComponent],
        providers: []
      })
        .overrideTemplate(ListadetComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ListadetComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ListadetService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Listadet(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.listadets[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
