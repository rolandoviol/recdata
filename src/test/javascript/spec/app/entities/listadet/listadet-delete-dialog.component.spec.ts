/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RecdataTestModule } from '../../../test.module';
import { ListadetDeleteDialogComponent } from 'app/entities/listadet/listadet-delete-dialog.component';
import { ListadetService } from 'app/entities/listadet/listadet.service';

describe('Component Tests', () => {
  describe('Listadet Management Delete Component', () => {
    let comp: ListadetDeleteDialogComponent;
    let fixture: ComponentFixture<ListadetDeleteDialogComponent>;
    let service: ListadetService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [ListadetDeleteDialogComponent]
      })
        .overrideTemplate(ListadetDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ListadetDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ListadetService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
