/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RecdataTestModule } from '../../../test.module';
import { ListadoDeleteDialogComponent } from 'app/entities/listado/listado-delete-dialog.component';
import { ListadoService } from 'app/entities/listado/listado.service';

describe('Component Tests', () => {
  describe('Listado Management Delete Component', () => {
    let comp: ListadoDeleteDialogComponent;
    let fixture: ComponentFixture<ListadoDeleteDialogComponent>;
    let service: ListadoService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [ListadoDeleteDialogComponent]
      })
        .overrideTemplate(ListadoDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ListadoDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ListadoService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
