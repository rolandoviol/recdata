/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RecdataTestModule } from '../../../test.module';
import { ListadoUpdateComponent } from 'app/entities/listado/listado-update.component';
import { ListadoService } from 'app/entities/listado/listado.service';
import { Listado } from 'app/shared/model/listado.model';

describe('Component Tests', () => {
  describe('Listado Management Update Component', () => {
    let comp: ListadoUpdateComponent;
    let fixture: ComponentFixture<ListadoUpdateComponent>;
    let service: ListadoService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [ListadoUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(ListadoUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(ListadoUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(ListadoService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Listado(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Listado();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
