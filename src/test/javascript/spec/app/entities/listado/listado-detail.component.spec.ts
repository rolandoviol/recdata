/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecdataTestModule } from '../../../test.module';
import { ListadoDetailComponent } from 'app/entities/listado/listado-detail.component';
import { Listado } from 'app/shared/model/listado.model';

describe('Component Tests', () => {
  describe('Listado Management Detail Component', () => {
    let comp: ListadoDetailComponent;
    let fixture: ComponentFixture<ListadoDetailComponent>;
    const route = ({ data: of({ listado: new Listado(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [ListadoDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(ListadoDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(ListadoDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.listado).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
