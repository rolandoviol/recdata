/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { RecdataTestModule } from '../../../test.module';
import { FormularioDeleteDialogComponent } from 'app/entities/formulario/formulario-delete-dialog.component';
import { FormularioService } from 'app/entities/formulario/formulario.service';

describe('Component Tests', () => {
  describe('Formulario Management Delete Component', () => {
    let comp: FormularioDeleteDialogComponent;
    let fixture: ComponentFixture<FormularioDeleteDialogComponent>;
    let service: FormularioService;
    let mockEventManager: any;
    let mockActiveModal: any;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [FormularioDeleteDialogComponent]
      })
        .overrideTemplate(FormularioDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FormularioDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FormularioService);
      mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
      mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
    });
  });
});
