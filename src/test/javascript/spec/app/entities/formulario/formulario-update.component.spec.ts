/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RecdataTestModule } from '../../../test.module';
import { FormularioUpdateComponent } from 'app/entities/formulario/formulario-update.component';
import { FormularioService } from 'app/entities/formulario/formulario.service';
import { Formulario } from 'app/shared/model/formulario.model';

describe('Component Tests', () => {
  describe('Formulario Management Update Component', () => {
    let comp: FormularioUpdateComponent;
    let fixture: ComponentFixture<FormularioUpdateComponent>;
    let service: FormularioService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [FormularioUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(FormularioUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FormularioUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FormularioService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Formulario(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Formulario();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
