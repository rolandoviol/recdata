/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecdataTestModule } from '../../../test.module';
import { FormularioDetailComponent } from 'app/entities/formulario/formulario-detail.component';
import { Formulario } from 'app/shared/model/formulario.model';

describe('Component Tests', () => {
  describe('Formulario Management Detail Component', () => {
    let comp: FormularioDetailComponent;
    let fixture: ComponentFixture<FormularioDetailComponent>;
    const route = ({ data: of({ formulario: new Formulario(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [FormularioDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(FormularioDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(FormularioDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.formulario).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
