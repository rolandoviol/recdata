/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { RecdataTestModule } from '../../../test.module';
import { FormularioComponent } from 'app/entities/formulario/formulario.component';
import { FormularioService } from 'app/entities/formulario/formulario.service';
import { Formulario } from 'app/shared/model/formulario.model';

describe('Component Tests', () => {
  describe('Formulario Management Component', () => {
    let comp: FormularioComponent;
    let fixture: ComponentFixture<FormularioComponent>;
    let service: FormularioService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [FormularioComponent],
        providers: []
      })
        .overrideTemplate(FormularioComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(FormularioComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(FormularioService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Formulario(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.formularios[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
