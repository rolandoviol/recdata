/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RecdataTestModule } from '../../../test.module';
import { MuestraDetailComponent } from 'app/entities/muestra/muestra-detail.component';
import { Muestra } from 'app/shared/model/muestra.model';

describe('Component Tests', () => {
  describe('Muestra Management Detail Component', () => {
    let comp: MuestraDetailComponent;
    let fixture: ComponentFixture<MuestraDetailComponent>;
    const route = ({ data: of({ muestra: new Muestra(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [MuestraDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(MuestraDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(MuestraDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should call load all on init', () => {
        // GIVEN

        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.muestra).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
