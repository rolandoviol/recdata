/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { Observable, of } from 'rxjs';

import { RecdataTestModule } from '../../../test.module';
import { MuestraUpdateComponent } from 'app/entities/muestra/muestra-update.component';
import { MuestraService } from 'app/entities/muestra/muestra.service';
import { Muestra } from 'app/shared/model/muestra.model';

describe('Component Tests', () => {
  describe('Muestra Management Update Component', () => {
    let comp: MuestraUpdateComponent;
    let fixture: ComponentFixture<MuestraUpdateComponent>;
    let service: MuestraService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [MuestraUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(MuestraUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MuestraUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MuestraService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new Muestra(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new Muestra();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
