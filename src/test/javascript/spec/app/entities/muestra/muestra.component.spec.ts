/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { RecdataTestModule } from '../../../test.module';
import { MuestraComponent } from 'app/entities/muestra/muestra.component';
import { MuestraService } from 'app/entities/muestra/muestra.service';
import { Muestra } from 'app/shared/model/muestra.model';

describe('Component Tests', () => {
  describe('Muestra Management Component', () => {
    let comp: MuestraComponent;
    let fixture: ComponentFixture<MuestraComponent>;
    let service: MuestraService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [MuestraComponent],
        providers: []
      })
        .overrideTemplate(MuestraComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(MuestraComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(MuestraService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Muestra(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.muestras[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
