/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { RecdataTestModule } from '../../../test.module';
import { RegistroComponent } from 'app/entities/registro/registro.component';
import { RegistroService } from 'app/entities/registro/registro.service';
import { Registro } from 'app/shared/model/registro.model';

describe('Component Tests', () => {
  describe('Registro Management Component', () => {
    let comp: RegistroComponent;
    let fixture: ComponentFixture<RegistroComponent>;
    let service: RegistroService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [RecdataTestModule],
        declarations: [RegistroComponent],
        providers: []
      })
        .overrideTemplate(RegistroComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(RegistroComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(RegistroService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new Registro(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.registros[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
